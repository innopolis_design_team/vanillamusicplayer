package ch.blinkenlights.artist;

public class ArtistDto {

    public ArtistDto(){}

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
