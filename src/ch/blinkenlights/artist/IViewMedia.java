package ch.blinkenlights.artist;

/**
 * Created by andre on 14.10.2016.
 */

public interface IViewMedia {

    void  startLoading();
    void  endLoading();
}
