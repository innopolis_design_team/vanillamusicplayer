package ch.blinkenlights.artist;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.MediaFetcher;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.objects.MediaLoader;


public class ArtistStorageAsyncTask extends AsyncTask<Object, Media, Void> {

    Activity activity;
    IMediaImageLoader loader;

    public ArtistStorageAsyncTask(Activity activity, IMediaImageLoader loader){

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected Void doInBackground(Object... params) {

        Object o1 = params[0];

        List<Media> listMedias = (List<Media>) o1;

        for (Media item : listMedias){

            Media m = MediaLoader.getFullMediaInfo(item.getPath());

            if (m.getArtwork() != null){

                item.setArtwork(m.getArtwork());

                publishProgress(item);
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

    }

    @Override
    protected void onProgressUpdate(Media... values) {
        super.onProgressUpdate(values);

        Media item = values[0];
        loader.addImageMedia(item);
    }
}
