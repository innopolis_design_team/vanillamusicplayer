package ch.blinkenlights.artist;

import ch.blinkenlights.activities.Media;

/**
 * Created by andre on 09.11.2016.
 */

public interface IMediaImageLoader {

    void addImageMedia(Media item);
}
