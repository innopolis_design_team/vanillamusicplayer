package ch.blinkenlights.artist;


import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;

public class ArtistCasheAsyncTask extends AsyncTask<Object, List<Media>, List<Media>> {

    Activity activity;
    IMediaLoader loader;

    public ArtistCasheAsyncTask(Activity activity, IMediaLoader loader) {

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected List<Media> doInBackground(Object... params) {

        Object o1 = params[0];

        List<Media> listAll = (List<Media>) o1;

        MediaStorage storage = MediaStorage.getInstance(activity);
        List<Media> list = storage.getArtists(listAll);
        return list;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(List<Media> result) {
        super.onPostExecute(result);

        loader.showMedia(result);
        loader.onLoadingFinish();
    }

}

