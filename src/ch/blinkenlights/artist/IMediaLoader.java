package ch.blinkenlights.artist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ch.blinkenlights.activities.Media;

/**
 * Created by andre on 14.10.2016.
 */

public interface IMediaLoader {

    @Nullable
    View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    void addMedia(Media item);
    void onLoadingFinish();
    void showMedia(List<Media> list);
}

