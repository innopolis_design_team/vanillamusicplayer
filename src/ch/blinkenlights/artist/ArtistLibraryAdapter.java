package ch.blinkenlights.artist;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;

public class ArtistLibraryAdapter extends BaseAdapter {

    private List<Media> medias;
    private LayoutInflater layoutInflater;
    private Context context;

    ArtistLibraryAdapter(Context context, List<Media> medias) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = medias;
        this.context=context;
    }

    ArtistLibraryAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = new ArrayList<>();
        this.context=context;
    }

    public void addMedia(Media item) {
        this.medias.add(item);
    }

    public void addAllMedia(List<Media> list) {

        this.medias.addAll(list);
    }

    public List<Media> getList() {
        return medias;
    }

    @Override
    public int getCount() {
        return medias.size();
    }

    @Override
    public Media getItem(int position) {
        return medias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView picture;
        TextView name;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_artist_item, parent, false);
            convertView.setTag(R.id.artist_picture, convertView.findViewById(R.id.artist_picture));
            convertView.setTag(R.id.artist_text, convertView.findViewById(R.id.artist_text));
        }

        picture = (ImageView) convertView.getTag(R.id.artist_picture);
        name = (TextView) convertView.getTag(R.id.artist_text);

        Media media = getItem(position);
        if (media.getArtwork()==null)
            picture.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.empty_album2));
        else
            picture.setImageBitmap(media.getArtwork());
        name.setText(media.getArtist());
        return convertView;
    }

    public List<Media> getCurrentPlaylist() {
        return new ArrayList<Media>(this.medias);
    }

    public void setCurrentPlaylist(List<Media> playlist) {

        if (playlist == null) {

            throw new IllegalArgumentException();

        }

        this.medias.clear();
        this.medias.addAll(playlist);
        notifyDataSetChanged();

    }

    public void addImageMedia(Media media) {

        for (int i = 0; i < medias.size(); i++){

            Media item = medias.get(i);
            if (item.getPath().equalsIgnoreCase(media.getPath())){

                item.setArtwork(media.getArtwork());
                medias.set(i, item);

                notifyDataSetChanged();
            }

        }
    }
}
