package ch.blinkenlights.player;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.objects.MediaLoader;
import ch.blinkenlights.objects.VanMediaPlayer;
import ch.blinkenlights.song.ListSongFragment;
import ch.blinkenlights.song.OnPlayList;

/**
 * Created by galie on 31.10.2016.
 */

public class FullPlayer extends AppCompatActivity implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, OnPlayList {

    private VanMediaPlayer player;
    private ImageButton playBtn;
    private ImageButton prevBtn;
    private ImageButton nextBtn;
    private ImageButton shuffleBtn;
    private ImageButton repeatBtn;
    private ImageView albumArtImg;
    private SeekBar progressBar;
    private TextView currentTime;
    private TextView overallTime;
    private TextView artistName;
    private TextView songName;
    private Media item;

    private Boolean inSeekMode = false;
    private Object lock = new Object();

    private final int REFRESH_INTERVAL = 100;
    public static String FULL_ITEM = "current_position";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullplayer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        player = VanMediaPlayer.getInstance(FullPlayer.this, this, this, this);

        player.addPlayList((ArrayList<Media>) AppContext.getCurrentPlayList());

        setSupportActionBar(toolbar);
        Bundle b = getIntent().getExtras();

        item = null;
        if (b != null && b.get(ListSongFragment.MEDIA_SONG) != null) {

            item = (Media) b.get(ListSongFragment.MEDIA_SONG);

            item.setArtwork(MediaLoader.getFullMediaInfo(item.getPath()).getArtwork());
        }

        artistName = (TextView) findViewById(R.id.fullp_artist);
        albumArtImg = (ImageView) findViewById(R.id.fullp_albumart);
        songName = (TextView) findViewById(R.id.fullp_title);
        setSongViewData(item);
        controlListeners();

        final SeekBar progressBar = (SeekBar) findViewById(R.id.fullp_seekbar);
        progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                player.trySeekTo(seekBar.getProgress());
                inSeekMode = false;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                inSeekMode = true;

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }
        });

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                synchronized (lock) {
                    while (true) {
                        handler.post(new Runnable() {
                            public void run() {
                                updatePlayerActivity();
                            }
                        });
                        try {
                            lock.wait(REFRESH_INTERVAL);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        new Thread(runnable).start();
    }

    public void updatePlayerActivity() {

        if (!inSeekMode) {

            final SeekBar progressBar = (SeekBar) findViewById(R.id.fullp_seekbar);
            final TextView currentTime = (TextView) findViewById(R.id.fullp_current_time);
            final TextView overallTime = (TextView) findViewById(R.id.fullp_overall_time);

            progressBar.setProgress(player.getProgress());
            currentTime.setText(getFormattedTime(player.getTimeElapsed()));
            overallTime.setText(getFormattedTime(player.getDuration()));

        }

    }

    public void setSongViewData(Media item) {

        this.item = item;

        if (item.getArtwork() == null){

            item.setArtwork(MediaLoader.getFullMediaInfo(item.getPath()).getArtwork());
        }

        if (item.isPlay()) {
            player.playOrPause(item.getPath());
            player.setCurrentPlayPosition(item.getCurrentPlayPosition());
        }

        int pos = player.getCurrentPlayPosition();

        artistName.setText(item.getArtist());
        songName.setText(item.getTitle());
        Bitmap artWork = item.getArtwork();
        if (artWork != null) {
            albumArtImg.setImageBitmap(artWork);
        } else {
            Bitmap emptyCover = BitmapFactory.decodeResource(getResources(),R.drawable.empty_album2);
            albumArtImg.setImageBitmap(emptyCover);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            item.setArtwork(null);
            int currentPosition = player.getCurrentPlayPosition();

            item.setCurrentPlayPosition(currentPosition);
            Intent intent = new Intent();
            intent.putExtra(FULL_ITEM, item);
            item.setPlay(player.isPlaying());
            setResult(RESULT_OK, intent);
            player.stop();
            finish();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

   /* @Override
    public void onDestroy() {
        super.onDestroy();

        if (player != null) {

            player.onDestroy();
        }
    }*/

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

    }

    private void controlListeners() {
        albumArtImg = (ImageView) findViewById(R.id.fullp_albumart);
        playBtn = (ImageButton) findViewById(R.id.fullp_play);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  Toast.makeText(FullPlayer.this, "Play", Toast.LENGTH_SHORT).show();
                player.playOrPause(item.getPath());

                if (item.getCurrentPlayPosition() > 0) {
                    
                    player.setCurrentPlayPosition(item.getCurrentPlayPosition());
                }

                if (player.isPlaying()) {
                    playBtn.setBackgroundColor(Color.TRANSPARENT);
                    playBtn.setBackgroundResource(R.drawable.pause_circle);
                } else {
                    playBtn.setBackgroundColor(Color.TRANSPARENT);
                    playBtn.setBackgroundResource(R.drawable.play_circle);
                }
            }
        });

        nextBtn = (ImageButton) findViewById(R.id.fullp_next);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.playNextSong();

             //   Toast.makeText(FullPlayer.this, "Next", Toast.LENGTH_SHORT).show();
            }
        });

        prevBtn = (ImageButton) findViewById(R.id.fullp_prev);
        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.playPreviousSong();

              //  Toast.makeText(FullPlayer.this, "Prev", Toast.LENGTH_SHORT).show();
            }
        });

        shuffleBtn = (ImageButton) findViewById(R.id.shuffle_fp);


        repeatBtn = (ImageButton) findViewById(R.id.repeat_fp);

        if (player.isPlaying()) {
            playBtn.setBackgroundColor(Color.TRANSPARENT);
            playBtn.setBackgroundResource(R.drawable.pause_circle);
        } else {
            playBtn.setBackgroundColor(Color.TRANSPARENT);
            playBtn.setBackgroundResource(R.drawable.play_circle);
        }

    }

    private static String getFormattedTime(int seconds) {

        String formattedTime = "";

        try {

            int hours = seconds / 3600;
            int remainder = seconds - hours * 3600;
            int minutes = remainder / 60;
            remainder = remainder - minutes * 60;

            formattedTime = String.format("%02d", minutes) + ":" + String.format("%02d", remainder);

            if (hours > 0) {

                formattedTime = String.format("%02d", hours) + ":" + formattedTime;

            }

        } catch (Exception ex) {

            formattedTime = "0:00";

        }

        return formattedTime;

    }

    @Override
    public void playSong(Media item) {

        setSongViewData(item);
    }
}