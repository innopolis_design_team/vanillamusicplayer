package ch.blinkenlights;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.playlist.PlayListDto;

public class ShPrefUtils {

    private static String FILE_NAME = "VanMusicPlayer";
    private static String JSON_PLAYLIST = "JSON_PLAYLIST";

    public static void savePlayList(Context context, String tagName, ArrayList<Media> list){

        Gson gson = new Gson();

        HashMap<String, String> map = getPlayLists(context);

        String jsonList =  gson.toJson(list);

        map.put(tagName, jsonList);

        String json =gson.toJson(map);

        SharedPreferences sd = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sd.edit();

        editor.putString(JSON_PLAYLIST, json);

        editor.commit();
    }

    public static ArrayList<Media> getPlayListByTagName(Context context, String tagName){

        Gson gson = new Gson();

        HashMap<String, String> map = getPlayLists(context);

        ArrayList<Media> list = null;

        if (map.containsKey(tagName)) {

            String json = map.get(tagName);

            list = gson.fromJson(json,
                    new TypeToken<ArrayList<Media>>() {
                    }.getType());

        }
        else {

            list = new ArrayList<>();
        }

        return list;

    }

    public static List<PlayListDto> getActionPlayLists(Context context){

        List<PlayListDto> listPlayLists = new ArrayList<>();

        HashMap<String, String> map = getPlayLists(context);

        Set<String> keySet = map.keySet();

        PlayListDto item = null;

        for (String key : keySet){

            if (isTagAction(context, key)) {

                item = new PlayListDto();
                item.setFullName("[" + key.toUpperCase() + "] " + " Playlist");
                item.setShortName(key);

                listPlayLists.add(item);
            }

        }

        return listPlayLists;
    }

    public static List<PlayListDto> getMoodPlayLists(Context context){

        List<PlayListDto> listPlayLists = new ArrayList<>();

        HashMap<String, String> map = getPlayLists(context);

        Set<String> keySet = map.keySet();

        PlayListDto item = null;

        for (String key : keySet){

            if (isTagMood(context, key)) {

                item = new PlayListDto();
                item.setFullName("[" + key.toUpperCase() + "] " + " Playlist");
                item.setShortName(key);

                listPlayLists.add(item);
            }

        }

        return listPlayLists;

    }

    private static boolean isTagAction(Context context, String tagName){

        String [] array = context.getResources().getStringArray(R.array.action_names);

        for (String name : array){

            if (name.equalsIgnoreCase(tagName)){

                return true;
            }
        }

        return false;
    }

    private static boolean isTagMood(Context context, String tagName){

        String [] array = context.getResources().getStringArray(R.array.moods_names);

        for (String name : array){

            if (name.equalsIgnoreCase(tagName)){

                return true;
            }
        }

        return false;
    }


    public static HashMap<String, String> getPlayLists(Context context){

        HashMap<String, String> map = new HashMap();

        SharedPreferences sd = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        String json =  sd.getString(JSON_PLAYLIST, null);

        if (json != null) {

            Gson gson = new Gson();

            map = gson.fromJson(json, HashMap.class);
        }

        return map;
    }
}
