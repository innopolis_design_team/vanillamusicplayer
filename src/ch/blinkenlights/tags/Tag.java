package ch.blinkenlights.tags;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Oleg on 18.10.2016.
 */

public class Tag implements Serializable {

    private UUID id;
    private String name;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tag(String name) {

        this(name, UUID.randomUUID());

    }

    public Tag(String name, UUID id) {

        this.id = id;
        this.name = name;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        return id.equals(tag.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

}
