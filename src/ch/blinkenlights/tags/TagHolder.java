package ch.blinkenlights.tags;

import ch.blinkenlights.activities.*;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.song.SongDto;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Oleg on 18.10.2016.
 */

public class TagHolder {

    private static final String CLASS_TAG = "TagHolder";

    private static HashSet<Tag> tags;
    private static HashMap<String, ArrayList<Tag>> taggedSongs;

    public static HashSet<Tag> getTags() {
        return (tags != null) ? tags : new HashSet<Tag>();
    }

    public static ArrayList<String> getTagsTitle() {
        ArrayList<String> tagsTitles = new ArrayList<>();
        for (Tag t:tags) {
            tagsTitles.add(t.getName());
        }
        return tagsTitles;
    }

    // the constructor is private, so there cannot be instances of this class
    private TagHolder() {
    }

    public static void Initialize() {

        tags = new HashSet<Tag>();

        tryReadTags();

        tags.add(new Tag("Walking", UUID.fromString("7b936334-4aba-42c0-a40c-5ac6a0d14cfc")));
        tags.add(new Tag("Jogging", UUID.fromString("ef5864f0-733b-447b-8b03-7a957a75cc40")));
        tags.add(new Tag("Still", UUID.fromString("dbc0c9f3-fcec-404a-b65a-5011545456a1")));
        tags.add(new Tag("Tilting", UUID.fromString("9ac7fcb4-00ef-4818-839b-5f840003d4f3")));
        tags.add(new Tag("On bicycle", UUID.fromString("c8160ee7-4c90-405f-a9b8-2a258f5afc26")));
        tags.add(new Tag("On vehicle", UUID.fromString("af32debb-131d-431e-b9f9-0ec055f05e23")));

    }

    private static boolean trySaveTags() {
        try {
            File tagsFile = new File("tags.data");

            if (!tagsFile.isFile()) {
                try {

                    tagsFile.createNewFile();

                } catch (Exception ex) {

                    Log.d(CLASS_TAG, "Error occurred while saving tags: " + ex.getMessage());
                    return false;

                }
            }

            FileOutputStream fos = new FileOutputStream(tagsFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(tags);
            oos.close();
            fos.close();

        } catch (IOException ioe) {

            Log.d(CLASS_TAG, "Error occurred while saving tags: " + ioe.getMessage());
            return false;

        }

        return true;
    }

    private static boolean tryReadTags() {

        try {

            File tagsFile = new File("tags.data");

            if (!tagsFile.isFile()) {

                throw new IOException("Could not find or read the \"tags.data\" file.");

            }

            FileInputStream fileInputStream = new FileInputStream(tagsFile);
            ObjectInputStream objectIntputStream = new ObjectInputStream(fileInputStream);
            tags = (HashSet<Tag>) objectIntputStream.readObject();
            objectIntputStream.close();
            fileInputStream.close();

        } catch (IOException ioe) {

            Log.d(CLASS_TAG, "Error occurred while reading tags: " + ioe.getMessage());
            return false;

        } catch (ClassNotFoundException cnfe) {

            Log.d(CLASS_TAG, "Error occurred while reading tags: " + cnfe.getMessage());
            return false;

        }

        return true;
    }

    public static List<Media> getPlaylistByTag(Tag tag, Activity activity) {

        MediaFetcher mediaFetcher = new MediaFetcher();

        List<Media> playlist = new ArrayList<Media>();

        ContentResolver cr = activity.getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);
        int count = 0;

        Set<String> hs = new HashSet<>();

        if (cur != null) {
            count = cur.getCount();

            if (count > 0) {
                while (cur.moveToNext()) {
                    String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                    mediaFetcher.setDataSource(data);

                    Media song = new Media(
                            mediaFetcher.getAlbum(),
                            mediaFetcher.getArtwork(),
                            mediaFetcher.getArtist(),
                            mediaFetcher.getTitle(),
                            mediaFetcher.getGenre(),
                            mediaFetcher.getYear(),
                            mediaFetcher.getDataSource()
                    );

                    playlist.add(song);

                    if (!hs.contains(song.getAlbum())) {

                        hs.add(mediaFetcher.getAlbum());
                    }
                }

            }
        }
        cur.close();

        //AppContext.setListMedias(playlist);

        return playlist;

    }

    public static boolean tagSongs(List<SongDto> songDtos, Tag tag) {

        if (songDtos == null || tag == null) {

            // throw an exception?
            return false;

        }

        for (SongDto song : songDtos)
        {
            song.addTag(tag);
        }

        return true;

    }

}
