package ch.blinkenlights.playlist;


public class PlayListDto {

    public PlayListDto(){}

    private String fullName;
    private String shortName;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}

