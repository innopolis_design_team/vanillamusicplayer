package ch.blinkenlights.playlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.tracking.TrackingDto;

public class PlayListAdapter extends BaseAdapter {

    public enum FilterMode { SONG, ARTIST, ALBUM, ALL };

    public PlayListAdapter(Context context, List<PlayListDto> list){

        this.context = context;
        this.list = list;
    }

    private Context context;
    private List<PlayListDto> list;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PlayListDto getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.play_list_item,
                    parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.tvName = (TextView) convertView
                    .findViewById(R.id.play_list_item_text);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        PlayListDto item = getItem(position);

        holder.tvName.setText(item.getFullName());

        return convertView;
    }

    public PlayListDto getTopPlayList(){

        if (this.list.size() > 0){

            return this.list.get(0);
        }
        return null;
    }

    public void changeTagsPositions(List<TrackingDto> listTrackingDtos){


        Collections.sort(listTrackingDtos, new Comparator<TrackingDto>() {
            @Override
            public int compare(TrackingDto o1, TrackingDto o2) {
                Integer p1 = o1.getConfidence();
                Integer p2 = o2.getConfidence();
                return (-1) * p1.compareTo(p2);
            }
        });

        List<PlayListDto> listPlayList = new ArrayList<>();

        for (TrackingDto trackingDto : listTrackingDtos){

            PlayListDto playListDto = getPlayListByShortName(trackingDto.getName());

            listPlayList.add(playListDto);
        }

        listPlayList = findOtherUnusedPlaylist(this.list, listPlayList);

        this.list.clear();

        this.list.addAll(listPlayList);

        notifyDataSetChanged();
    }

    private List<PlayListDto> findOtherUnusedPlaylist(List<PlayListDto> oldList, List<PlayListDto> newList){

        List<PlayListDto> list = new ArrayList<>();

        list.addAll(newList);

        for (PlayListDto item : oldList){

            if (!isInListExists(item, newList)){

                list.add(item);
            }
        }

        return list;
    }

    private boolean isInListExists(PlayListDto playListDto, List<PlayListDto> listDtos){

        for (PlayListDto item : listDtos){

            if (item.getShortName().equalsIgnoreCase(playListDto.getShortName())){

                return true;
            }
        }
        return false;

    }

    private PlayListDto getPlayListByShortName(String name){

        for (PlayListDto item : this.list){

            if (item.getShortName().equalsIgnoreCase(name)){

                return item;
            }
        }
        return new PlayListDto();

    }

    public List<String> getTagsNames(){

        List<String> listNames = new ArrayList<>();

        for (PlayListDto item : list){

            listNames.add(item.getShortName());
        }

        return listNames;
    }

    public static List<Media> filterPlaylist(List<Media> playlist, String filterString, FilterMode mode) {

        if (playlist == null || filterString == null) {

            throw new IllegalArgumentException();

        }

        ArrayList<Media> filteredPlaylist = new ArrayList<Media>();

        for (Media song : playlist)
        {
            if (matchesFilter(song, filterString, mode))
            {
                filteredPlaylist.add(song);
            }
        }

        return  filteredPlaylist;

    }

    public static List<Media> filterPlaylist(List<Media> playlist, String filterString) {

        return  filterPlaylist(playlist, filterString, FilterMode.ALL);

    }

    private static boolean matchesFilter(Media song, String filterString, FilterMode mode) {

        if (song == null || filterString == null) {

            throw new IllegalArgumentException();

        }

        filterString = filterString.toLowerCase();

        Boolean match = false;

        switch (mode)
        {
            case SONG:
                match =    song.getTitle()  != null && song.getTitle().toLowerCase().contains(filterString);
                break;
            case ARTIST:
                match =    song.getArtist() != null && song.getArtist().toLowerCase().contains(filterString);
                break;
            case ALBUM:
                match =    song.getAlbum()  != null && song.getAlbum().toLowerCase().contains(filterString);
                break;
            case ALL:
                match =    song.getTitle()  != null && song.getTitle().toLowerCase().contains(filterString)
                        || song.getAlbum()  != null && song.getAlbum().toLowerCase().contains(filterString)
                        || song.getArtist() != null && song.getArtist().toLowerCase().contains(filterString);
                break;
        }

        return match;

    }

    static class ViewHolder {

        TextView tvName;
    }
}

