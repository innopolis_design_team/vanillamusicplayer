package ch.blinkenlights.playlist;

import android.app.ListFragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.android.vanilla.LibraryActivity;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.song.ListTagSongFragment;


public class ListPlayListFragment  extends ListFragment {

    private PlayListAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ActionBarActivity activity = (ActionBarActivity) getActivity();

        List<PlayListDto> listPlayLists = ShPrefUtils.getMoodPlayLists(getActivity());

        adapter = new PlayListAdapter(getActivity(), listPlayLists);

        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String tagName = adapter.getItem(position).getShortName();

        ListTagSongFragment fragment = ListTagSongFragment.newInstance(tagName);

        FragmentHelper.setFragmentWithBackStack(getActivity(), fragment);

    }
}

