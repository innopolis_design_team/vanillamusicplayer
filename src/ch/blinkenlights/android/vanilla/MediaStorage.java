package ch.blinkenlights.android.vanilla;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import ch.blinkenlights.activities.Media;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * Created by andre on 14.10.2016.
 */

public class MediaStorage {

    private static String FILE_NAME = "VaniliaMusicPlayer";
    private static String MEDIA_JSON = "media_json";

    private static Context context;

    private static MediaStorage instance;

    private static Gson gson;

    private MediaStorage(){}

    public static MediaStorage getInstance(Context ctx){

        if (instance == null){

            instance = new MediaStorage();
        }
        context = ctx;

        gson = new Gson();

        return instance;
    }

    public  void saveMedia(List<Media> list){

        SharedPreferences sd = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sd.edit();

        String json = gson.toJson(list);

        editor.putString(MEDIA_JSON, json);

        editor.commit();


    }

    public  List<Media> getAllMedia(){

        SharedPreferences sd = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        String json = sd.getString(MEDIA_JSON, null);

        if (json == null) { return null;}

        Type listType = new TypeToken<List<Media>>(){}.getType();

        List<Media> list = gson.fromJson(json,listType );

        return list;
    }

    public  List<Media> getArtists(){

        List<Media> listAll = getAllMedia();
        List<Media> listArtist = getAllMedia();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String artistName = m.getArtist();

            if (!hs.contains(artistName)){

                listArtist.add(m);
            }
        }

        return listArtist;
    }

    public  List<Media> getArtists(List<Media> listAll){

        List<Media> listArtist = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String artistName = m.getArtist();

            if (!hs.contains(artistName)){

                listArtist.add(m);
                hs.add(artistName);
            }
        }

        return listArtist;
    }

    public  List<Media> getAlbumsByArtistName(List<Media> listAll, String artistName){

        List<Media> listAlbums = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String albumName = m.getAlbum();

            String name = m.getArtist();

            if ( name != null && name.equalsIgnoreCase(artistName) && !hs.contains(albumName)){

                listAlbums.add(m);
                hs.add(albumName);
            }
        }

        return listAlbums;
    }

    public  List<Media> getAlbumsByGenreName(List<Media> listAll, String genreName4){

        List<Media> listAlbums = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String albumName = m.getAlbum();

            String name = m.getGenre();

            if ( name != null && name.equalsIgnoreCase(genreName4) && !hs.contains(albumName)){

                listAlbums.add(m);
                hs.add(albumName);
            }
        }

        return listAlbums;
    }

    public  List<Media> getSongsByAlbumName(List<Media> listAll, String albumName){

        List<Media> listSongs = new ArrayList<>();

        for (Media m : listAll){

            String name = m.getAlbum();

            if ( name != null && name.equalsIgnoreCase(albumName)){

                listSongs.add(m);
            }
        }

        return listSongs;
    }

    public  List<Media> getAlbums(List<Media> listAll){

        List<Media> listAlbums = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String albumName = m.getAlbum();

            if (!hs.contains(albumName)){

                listAlbums.add(m);
                hs.add(albumName);
            }
        }

        return listAlbums;
    }


    public List<Media> getGenres(List<Media> listAll) {

        List<Media> listGenres = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media m : listAll){

            String genreName = m.getGenre();

            if (!hs.contains(genreName)){

                listGenres.add(m);
                hs.add(genreName);
            }
        }

        return listGenres;
    }
}
