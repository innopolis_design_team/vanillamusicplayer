package ch.blinkenlights.objects;


import android.app.Activity;
import android.database.Cursor;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.MediaFetcher;

public class MediaLoader {


    public static List<Media> getArtists( List<Media> listMedias){

        List<Media> listArtists = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media item : listMedias){

            if (!hs.contains(item.getArtist())) {

                hs.add(item.getArtist());

                listArtists.add(item);
            }
        }

        return  listArtists;
    }

    public static List<Media> getGenres( List<Media> listMedias){

        List<Media> listArtists = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media item : listMedias){

            if (!hs.contains(item.getArtist())) {

                hs.add(item.getArtist());

                listArtists.add(item);
            }
        }

        return  listArtists;
    }

    public static List<Media> getAlbums( List<Media> listMedias){

        List<Media> listArtists = new ArrayList<>();

        Set<String> hs = new HashSet<>();

        for (Media item : listMedias){

            if (!hs.contains(item.getAlbum())) {

                hs.add(item.getAlbum());

                listArtists.add(item);
            }
        }

        return  listArtists;
    }

    public static List<Media> getListMedias(Activity activity) {

        List<Media> listMedias = new ArrayList<>();

        Media item = null;

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM
        };

        Cursor cursor = activity.managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);

        while (cursor.moveToNext()) {

            String id = cursor.getString(0);
            String title = cursor.getString(1);
            String path = cursor.getString(2);
            String name = cursor.getString(3);
            String duration = cursor.getString(4);
            String artist = cursor.getString(5);
            String album = cursor.getString(6);

            if (name == null || name.endsWith(".mp3")) {

                name = title;
            }

            item = new Media(album, null, artist, name, duration, 2016, path);
            listMedias.add(item);
        }

        return listMedias;
    }

    public static Media getFullMediaInfo(String path) {

        MediaFetcher mediaFetcher = new MediaFetcher();

        mediaFetcher.setDataSource(path);

        Media item = new Media(
                mediaFetcher.getAlbum(),
                mediaFetcher.getArtwork(),
                mediaFetcher.getArtist(),
                mediaFetcher.getTitle(),
                mediaFetcher.getGenre(),
                mediaFetcher.getYear(),
                mediaFetcher.getDataSource()
        );

        return item;


    }
}
