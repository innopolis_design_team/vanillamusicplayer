package ch.blinkenlights.objects;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import ch.blinkenlights.android.vanilla.R;

public class FragmentHelper {

    public static void setFragment(Activity activity, Fragment fragment){

        FragmentManager fragmentManager = activity.getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    public static void setFragmentWithBackStack(Activity activity, Fragment fragment){

        FragmentTransaction fragmentTransaction = activity.getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.content_frame, fragment);

        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }
}
