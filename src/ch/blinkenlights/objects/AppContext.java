package ch.blinkenlights.objects;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.MediaFetcher;


public class AppContext extends MultiDexApplication {

    private static List<Media> list;
    private static Context context;
    private static Gson mGson;
    private static List<Media> currentPlayList;

    public static Context getContext() {
        return context;
    }

    public static void setCurrentPlayList(List<Media> currentPlayList) {
        AppContext.currentPlayList = currentPlayList;
    }

    public static List<Media> getCurrentPlayList() {
        if (currentPlayList == null){

            return new ArrayList<>();
        }
        return currentPlayList;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        list = null;


        context = getApplicationContext();

        mGson = new Gson();
    }

    public static Gson getGson() {
        return mGson;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

        mGson = new Gson();
    }

    public static void setListMedias(List<Media> medias){

        list = medias;
    }



    public static void addImageToMedia(Media media){

        for (int i = 0; i < list.size(); i++){

            Media item = list.get(i);
            if (item.getPath().equalsIgnoreCase(media.getPath())){

                item.setArtwork(media.getArtwork());
                list.set(i, item);
            }

        }
    }

    public static List<Media> getListMedias(){

        return list;
    }
}
