package ch.blinkenlights.objects;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import java.util.ArrayList;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.song.OnPlayList;

public class VanMediaPlayer {

    private static  Context contex;

    private String playSongPath = null;

    private ArrayList<String> playList;
    private ArrayList<Media> playListMedia;

    private static MediaPlayer mediaPlayer;

    private static int songsIndex = -1;

    private static VanMediaPlayer sInstance;

    private static AudioManager am;

    private static boolean isPlaySong = false;

    private static MediaPlayer.OnPreparedListener prepListner;
    private static MediaPlayer.OnCompletionListener compltListner;

    private static OnPlayList onPlayListner;

    private VanMediaPlayer(){}


    //getINstance()

    public static VanMediaPlayer getInstance(Context ctx){

        contex = ctx;

        if (sInstance == null){

            sInstance = new VanMediaPlayer();
        }

        if (mediaPlayer == null){

            mediaPlayer = createNewMediaPlayer();

        }

        if (am == null){

            am = (AudioManager)contex.getSystemService(contex.AUDIO_SERVICE);
        }

        isPlaySong = false;

        return sInstance;
    }

    //----------------------------------------


    public static VanMediaPlayer getInstance(Context ctx,
                                             MediaPlayer.OnPreparedListener prepList,
                                             MediaPlayer.OnCompletionListener compList

    ){

        contex = ctx;

        if (sInstance == null){

            sInstance = new VanMediaPlayer();
        }

        if (mediaPlayer == null){

            mediaPlayer = createNewMediaPlayer(prepList, compList, onPlayListner);

        }

        if (am == null){

            am = (AudioManager)contex.getSystemService(contex.AUDIO_SERVICE);
        }

        prepListner = prepList;
        compltListner = compList;
        onPlayListner = null;

        isPlaySong = false;

        return sInstance;
    }


    //---------------------------------------------------------------


    public static VanMediaPlayer getInstance(Context ctx,
                                             MediaPlayer.OnPreparedListener prepList,
                                             MediaPlayer.OnCompletionListener compList,
                                             OnPlayList on_playList){
        contex = ctx;

        if (sInstance == null){

            sInstance = new VanMediaPlayer();
        }

        if (mediaPlayer == null){

            mediaPlayer = createNewMediaPlayer(prepList, compList, onPlayListner);

        }

        if (am == null){

            am = (AudioManager)contex.getSystemService(contex.AUDIO_SERVICE);
        }

        prepListner = prepList;
        compltListner = compList;

        onPlayListner = on_playList;

        isPlaySong = false;


        return sInstance;
    }

    //---------------------------------------


    //createNewMediaPlayer



    private static MediaPlayer createNewMediaPlayer(){

        mediaPlayer = new MediaPlayer();

        return mediaPlayer;
    }

    //----------------------------

    private static MediaPlayer createNewMediaPlayer(MediaPlayer.OnPreparedListener prepList,
                                                    MediaPlayer.OnCompletionListener compList,  OnPlayList on_playList){

        mediaPlayer = new MediaPlayer();


        if (prepList != null) {

            mediaPlayer.setOnPreparedListener(prepList);
        }

        if (compList != null) {

            mediaPlayer.setOnCompletionListener(compList);
        }

        if (on_playList != null){

            onPlayListner = on_playList;
        }

        return mediaPlayer;
    }

    //---------------------------



    public void playPreviousSong(){

        if (playList == null) {return;}


        if (playList.size() == 0){

            return;
        }

        else if (playList.size() == 1){


            stop();
            onPlayListner.playSong(playListMedia.get(0));
            play(playList.get(0));

        }

        else if (songsIndex <= 0){

            stop();
            onPlayListner.playSong(playListMedia.get(playList.size() - 1));
            play(playList.get(playList.size() - 1));

        }
        else{

            stop();
            onPlayListner.playSong(playListMedia.get(songsIndex - 1));
            play(playList.get(songsIndex - 1));

        }
    }

    public void playNextSong(){

        if (playList == null) {return;}


        if (playList.size() == 0){

            return;
        }

        else if (playList.size() == 1){

            stop();
            onPlayListner.playSong(playListMedia.get(0));
            play(playList.get(0));

        }

        else if (songsIndex == playList.size() - 1){

            stop();
            onPlayListner.playSong(playListMedia.get(0));
            play(playList.get(0));

        }
        else{

            stop();
            onPlayListner.playSong(playListMedia.get(songsIndex + 1));
            play(playList.get(songsIndex + 1));

        }
    }

    public void removePlayList(){

        if (playList != null) {

            this.playList.clear();
            this.playList = null;
        }

        songsIndex = -1;
        isPlaySong = false;

        prepListner = null;
        compltListner = null;
    }

    public void addPlayList(ArrayList<Media> list){

        this.playList = new ArrayList<>();
        this.playListMedia = list;
        for (Media item : list){

            this.playList.add(item.getPath());
        }
    }

    public int getCurrentPlayPosition(){

        if (isPlaying()){

            return mediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public void setCurrentPlayPosition(int position){

        mediaPlayer.seekTo(position);
    }

    public void playOrPause(String path){

        if (path == null || path.isEmpty()){ return;}

        if (playSongPath != null && !playSongPath.isEmpty()){

            if (!playSongPath.equalsIgnoreCase(path)){

                stop();
                playSongPath = path;
            }

        }
        else{

            playSongPath = path;
        }

        if (!isPlaySong){

            play(path);

            isPlaySong = true;
        }
        else if (mediaPlayer.isPlaying()){

            pause();
        }
        else{

            resume();
        }
    }

    public boolean isPlaying(){

        if (mediaPlayer == null){

            return false;
        }

        if (mediaPlayer.isPlaying()){

            return true;
        }
        else{

            return false;
        }

    }

    private void pause(){

        if (mediaPlayer == null) {return;}

        if (mediaPlayer.isPlaying()){

            mediaPlayer.pause();
        }

    }

    private void resume(){

        if (mediaPlayer == null) {return;}

        if (!mediaPlayer.isPlaying()) {

            mediaPlayer.start();
        }
    }

    public void stop(){

        if (mediaPlayer == null) {return;}

        mediaPlayer.stop();

        isPlaySong = false;

        playSongPath = null;
    }

    private void play(String path){

        if (playList != null){

            for (int i = 0; i < playList.size(); i++){

                String value = playList.get(i);

                if (value.equalsIgnoreCase(path)){

                    songsIndex = i;
                }
            }
        }

        releaseMP();
        try {

            if (mediaPlayer == null){

                mediaPlayer = createNewMediaPlayer(prepListner, compltListner, onPlayListner);
            }

            mediaPlayer.setDataSource(path);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();

            if (!mediaPlayer.isPlaying()) {

                mediaPlayer.start();
            }
        }catch (Exception e){}
    }

    public void releaseMP() {

        isPlaySong = false;

        if (mediaPlayer != null) {

            try {

                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void onDestroy(){

        releaseMP();
        removePlayList();
    }

    public int getProgress() {

        int currentPosition = 0;

        try {

            currentPosition = (int) Math.min(100, Math.max(0, 100.0 * mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()));

        }
        catch(Exception ex) {}

        return currentPosition;

    }

    public int getTimeElapsed() {

        int elapsedTime = 0;

        try {

            elapsedTime = mediaPlayer.getCurrentPosition() / 1000;

        }
        catch(Exception ex) {}

        return elapsedTime;

    }

    public int getDuration() {

        int elapsedTime = 0;

        try {

            elapsedTime = mediaPlayer.getDuration() / 1000;

        }
        catch(Exception ex) {}

        return elapsedTime;

    }

    public void trySeekTo(int progress) {

        try {

            mediaPlayer.seekTo((int) (0.01 * progress * mediaPlayer.getDuration()));

        } catch (Exception ex) {}

    }
}
