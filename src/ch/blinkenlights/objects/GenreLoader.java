package ch.blinkenlights.objects;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;

public class GenreLoader {

    private static Cursor mediaCursor;
    private static Cursor genresCursor;

    private static String[] mediaProjection = {
            MediaStore.Audio.Media._ID,//
            MediaStore.Audio.Media.TITLE,//
            MediaStore.Audio.Media.DATA,//
            MediaStore.Audio.Media.DISPLAY_NAME,//
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.ARTIST,//
            MediaStore.Audio.Media.ALBUM//
    };
    private static String[] genresProjection = {
            MediaStore.Audio.Genres.NAME,
            MediaStore.Audio.Genres._ID
    };

    public static List<Media> getGenresFromStorage(Context context) {

        Media item = null;
        List<Media> list = new ArrayList<>();

        List<String> genres = new ArrayList<>();

        mediaCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                mediaProjection, null, null, null);

        int artist_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST);
        int album_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media.ALBUM);
        int title_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE);

        int name_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME);

        int id_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media._ID);

        int path_column_index = mediaCursor
                .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);

        if (mediaCursor.moveToFirst()) {
            do {
                String info = "Song " + mediaCursor.getString(title_column_index) + " ";
                info += "from album " + mediaCursor.getString(album_column_index) + " ";
                info += "by " + mediaCursor.getString(artist_column_index) + ". ";

                int musicId = Integer.parseInt(mediaCursor.getString(id_column_index));

                Uri uri = MediaStore.Audio.Genres.getContentUriForAudioId("external", musicId);
                genresCursor = context.getContentResolver().query(uri,
                        genresProjection, null, null, null);
                int genre_column_index = genresCursor.getColumnIndexOrThrow(MediaStore.Audio.Genres.NAME);

                String name = mediaCursor.getString(name_column_index);

                if (name == null){

                    name = mediaCursor.getString(title_column_index);
                }

                Media media = new Media(mediaCursor.getString(album_column_index),
                        null,
                        mediaCursor.getString(artist_column_index),
                        name,
                        null,
                        0,
                        mediaCursor.getString(path_column_index)
                );

                if (genresCursor.moveToFirst()) {

                    String genreName = genresCursor.getString(genre_column_index);

                     media = new Media(mediaCursor.getString(album_column_index),
                            null,
                            mediaCursor.getString(artist_column_index),
                            name,
                            genreName,
                            0,
                            mediaCursor.getString(path_column_index)
                    );

                }

                list.add(media);

            } while (mediaCursor.moveToNext());
        }

        return list;
    }
}
