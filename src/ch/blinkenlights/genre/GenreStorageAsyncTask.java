package ch.blinkenlights.genre;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.objects.*;


public class GenreStorageAsyncTask extends AsyncTask<Object, Object, List<Media>> {

    Activity activity;
    IMediaLoader loader;

    public GenreStorageAsyncTask(Activity activity, IMediaLoader loader){

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected List<Media> doInBackground(Object... params) {

       List<Media> list =   ch.blinkenlights.objects.GenreLoader.getGenresFromStorage(activity);

        AppContext.setListMedias(list);

        Set<String> hs = new HashSet<>();

        List<Media> listGenres = new ArrayList<>();

        for (Media song : list) {

            if (song.getGenre() != null &&  !song.getGenre().isEmpty() && !hs.contains(song.getGenre())) {

                hs.add(song.getGenre());
                listGenres.add(song);
            }


        }

        return listGenres;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(List<Media> result) {
        super.onPostExecute(result);

        loader.onLoadingFinish();
        loader.showMedia(result);
    }


}