package ch.blinkenlights.genre;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.artist.IMediaImageLoader;
import ch.blinkenlights.objects.MediaLoader;

/**
 * Created by andre on 09.11.2016.
 */

public class GenreImageLoaderAsyncTask extends AsyncTask<Object, Media, Void> {

    Activity activity;
    IMediaImageLoader loader;

    public GenreImageLoaderAsyncTask(Activity activity, IMediaImageLoader loader){

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected Void doInBackground(Object... params) {

        Object o1 = params[0];

        List<Media> listMedias = (List<Media>) o1;

        for (Media item : listMedias){

            Media m = MediaLoader.getFullMediaInfo(item.getPath());

            if (m.getArtwork() != null){

                item.setArtwork(m.getArtwork());

                publishProgress(item);
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

    }

    @Override
    protected void onProgressUpdate(Media... values) {
        super.onProgressUpdate(values);

        Media item = values[0];
        loader.addImageMedia(item);
    }


}
