package ch.blinkenlights.genre;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.song.SongDto;

public class GenreAdapter extends BaseAdapter {

    private List<Media> medias;
    private LayoutInflater layoutInflater;

    GenreAdapter(Context context, List<Media> medias) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = medias;
    }

    public GenreAdapter(Context context) {

        this.layoutInflater = LayoutInflater.from(context);
        this.medias = new ArrayList<>();
    }

    public void addMedia(Media item){

        this.medias.add(item);
    }

    public void addAllMedia(List<Media> list){

        this.medias.addAll(list);
    }

    @Override
    public int getCount() {
        return medias.size();
    }

    @Override
    public Media getItem(int position) {
        return medias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView picture;
        TextView name;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.grid_genre_item, parent, false);
            convertView.setTag(R.id.genre_picture, convertView.findViewById(R.id.genre_picture));
            convertView.setTag(R.id.genre_text, convertView.findViewById(R.id.genre_text));
        }

        picture = (ImageView) convertView.getTag(R.id.genre_picture);
        name = (TextView) convertView.getTag(R.id.genre_text);

        Media media = getItem(position);
        picture.setImageBitmap(media.getArtwork());
        name.setText(media.getGenre());

        return convertView;
    }

    public void addImageMedia(Media media) {

        for (int i = 0; i < medias.size(); i++){

            Media item = medias.get(i);
            if (item.getPath().equalsIgnoreCase(media.getPath())){

                item.setArtwork(media.getArtwork());
                medias.set(i, item);

                notifyDataSetChanged();
            }

        }
    }
}

