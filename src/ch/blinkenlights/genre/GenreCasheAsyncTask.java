package ch.blinkenlights.genre;


import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;
import ch.blinkenlights.artist.IMediaLoader;

public class GenreCasheAsyncTask extends AsyncTask<Object, Object, List<Media>> {

    Activity activity;
    IMediaLoader loader;

    public GenreCasheAsyncTask(Activity activity, IMediaLoader loader){

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected List<Media> doInBackground(Object... params) {

        Object o1 = params[0];

        List<Media> listAll = (List<Media>) o1;

        MediaStorage storage = MediaStorage.getInstance(activity);
        List<Media> list = storage.getGenres(listAll);

        return list;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(List<Media> result) {
        super.onPostExecute(result);

        loader.onLoadingFinish();
        loader.showMedia(result);
    }
}