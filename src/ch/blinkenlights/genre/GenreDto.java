package ch.blinkenlights.genre;



public class GenreDto {

    public GenreDto (){}

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
