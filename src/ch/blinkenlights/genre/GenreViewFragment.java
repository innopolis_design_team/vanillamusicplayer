package ch.blinkenlights.genre;

import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.MediaFetcher;
import ch.blinkenlights.album.AlbumCasheAsyncTask;
import ch.blinkenlights.album.AlbumLibraryAdapter;
import ch.blinkenlights.album.AlbumStorageAsyncTask;
import ch.blinkenlights.album.AlbumViewFragment;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.ArtistLibraryAdapter;
import ch.blinkenlights.artist.IMediaImageLoader;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.artist.IViewMedia;
import ch.blinkenlights.objects.*;
import ch.blinkenlights.objects.GenreLoader;

/**
 * Created by galie on 13.10.2016.
 */

public class GenreViewFragment extends Fragment implements IMediaLoader, IMediaImageLoader, IViewMedia {

    private GridView gridView;
    private GenreAdapter adapter;

    private RelativeLayout relativeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_layout_main, container, false);


        gridView = (GridView) view.findViewById(R.id.gridView);

        relativeLayout = (RelativeLayout) view.findViewById(R.id.rlArtist);

        adapter = new GenreAdapter(view.getContext());

        gridView.setAdapter(adapter);

        List<Media> list = AppContext.getListMedias();


        if (list == null) {

            GenreStorageAsyncTask task = new GenreStorageAsyncTask(getActivity(), this);
            task.execute();
        }
        else{

            GenreCasheAsyncTask task = new GenreCasheAsyncTask(getActivity(), this);
            task.execute(list);
        }

        startLoading();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Media item = adapter.getItem(position);

                AlbumViewFragment fragment = AlbumViewFragment.newInstance( null, item.getGenre());

                ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Albums");
                FragmentHelper.setFragmentWithBackStack(getActivity(), fragment);

            }
        });

        return view;
    }

    @Override
    public void addMedia(Media item) {

        adapter.addMedia(item);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingFinish() {

        endLoading();
    }

    @Override
    public void showMedia(List<Media> list) {

        adapter.addAllMedia(list);
        adapter.notifyDataSetChanged();

        GenreImageLoaderAsyncTask task = new GenreImageLoaderAsyncTask(getActivity(), this);
        task.execute(list);


        endLoading();
    }

    @Override
    public void startLoading() {

        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void endLoading() {

        relativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void addImageMedia(Media item) {

        adapter.addImageMedia(item);
    }
}
