package ch.blinkenlights.listners;

import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;

public class CbMultSongsListner implements CheckBox.OnCheckedChangeListener {

    public static int MEDIA_KEY = 1;
    ArrayList<Media> list;

    public CbMultSongsListner(){

        list = new ArrayList<>();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        Media item = (Media) buttonView.getTag(R.id.cb_mult);

        if (item == null) {return;}

        if (isChecked == true){

            if (!isContains(item)){

                item.setSelected(true);

                item.setArtwork(null);
                list.add(item);
            }

        }
        else {

            if (isContains(item)){

                item.setSelected(false);
                list.remove(getIndex(item));
            }
        }

    }

    private boolean isContains(Media item){

        for (Media m : list){

           if (m.getPath().equalsIgnoreCase(item.getPath())){

               return true;
           }
        }
        return false;
    }

    private int getIndex(Media item){

        for (int i = 0; i < list.size(); i++){

            Media m = list.get(i);

            if (m.getPath().equalsIgnoreCase(item.getPath())){

                return i;
            }
        }
        return -1;
    }

    public void clearMedias(){

        list.clear();
        list = new ArrayList<>();
    }

    public ArrayList<Media> getListMedia() {
        return list;
    }
}
