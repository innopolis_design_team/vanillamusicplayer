package ch.blinkenlights.song;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.listners.CbMultSongsListner;


public class SongMultAdapter extends BaseAdapter {

    private List<Media> medias;
    private LayoutInflater layoutInflater;
    private Context context;
    private CbMultSongsListner cbListner;

    SongMultAdapter(Context context, List<Media> medias) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = medias;
    }

    SongMultAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = new ArrayList<>();
    }

    public SongMultAdapter(Context context, CbMultSongsListner cbListner) {
        this.context = context;
        this.cbListner = cbListner;
        this.medias = new ArrayList<>();
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void addMedia(Media item){

        this.medias.add(item);
    }

    public void addAllMedia(List<Media> list){

        this.medias.addAll(list);
    }

    @Override
    public int getCount() {
        return medias.size();
    }

    @Override
    public Media getItem(int position) {
        return medias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView artist;
        TextView title;
        LinearLayout ll;
        CheckBox cb;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.song_mult_item, parent, false);
            convertView.setTag(R.id.song_mult_item_title, convertView.findViewById(R.id.song_mult_item_title));
            convertView.setTag(R.id.song_mult_item_artist, convertView.findViewById(R.id.song_mult_item_artist));
            convertView.setTag(R.id.songs_mult_ll, convertView.findViewById(R.id.songs_mult_ll));
            convertView.setTag(R.id.cb_mult, convertView.findViewById(R.id.cb_mult));
        }

        title = (TextView) convertView.getTag(R.id.song_mult_item_title);
        artist = (TextView) convertView.getTag(R.id.song_mult_item_artist);
        ll = (LinearLayout) convertView.getTag(R.id.songs_mult_ll);
        cb = (CheckBox) convertView.getTag(R.id.cb_mult);

        Media item = getItem(position);
        artist.setText(item.getArtist());
        title.setText(item.getTitle());

        if ( item.getTitle() == null && item.getArtist() == null && item.getAlbum() != null){
           artist.setText(item.getAlbum());
        }

        cb.setTag(R.id.cb_mult, item);

        cb.setOnCheckedChangeListener(cbListner);

       cb.setChecked(item.isSelected());

        return convertView;
    }


    public void update(Media item, int position) {

        medias.set(position, item);

        notifyDataSetChanged();
    }

    public  void setSaveSongs(List<Media> listSongs){

        clearChecks();
        for (Media song : listSongs) {

            for (int i = 0; i < medias.size(); i++) {

                Media media = medias.get(i);

                if ( !medias.get(i).isSelected() && media.getPath().equalsIgnoreCase(song.getPath())){

                    medias.get(i).setSelected(true);
                }
            }
        }

        notifyDataSetChanged();
    }

    public  void clearChecks(){

        for (int i = 0; i < medias.size(); i++){

            medias.get(i).setSelected(false);
        }

        notifyDataSetChanged();
    }
}

