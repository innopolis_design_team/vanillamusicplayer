package ch.blinkenlights.song;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.TagSongActivity;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.artist.IViewMedia;
import ch.blinkenlights.listners.CbMultSongsListner;
import ch.blinkenlights.objects.AppContext;

public class ListMultiSongFragment extends Fragment implements IMediaLoader, IViewMedia {

    private static String TAG_NAME = "tag_name";
    private SongMultAdapter adapter;
    private ListView listView;
    private RelativeLayout relativeLayout;

    private CbMultSongsListner cbListner;

    public static ListMultiSongFragment newInstance(String tagName){

        Bundle b = new Bundle();
        b.putString(TAG_NAME, tagName);

        ListMultiSongFragment fragment = new ListMultiSongFragment();

        fragment.setArguments(b);

        return fragment;
    }

    public static ListMultiSongFragment newInstance(){

        ListMultiSongFragment fragment = new ListMultiSongFragment();

        return fragment;
    }

    private void goBack(String tagName, ArrayList<Media> list){

        if (getActivity() instanceof TagSongActivity){

            TagSongActivity activity = (TagSongActivity) getActivity();

            activity.goBack(tagName, list);
        }
    }

    private String tagName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_view_item, container, false);

        cbListner = new CbMultSongsListner();

        listView = (ListView) view.findViewById(R.id.listView);

        relativeLayout = (RelativeLayout) view.findViewById(R.id.rlArtist);

        adapter = new SongMultAdapter(getActivity(), cbListner);

        listView.setAdapter(adapter);


        List<Media> list = new ArrayList<>();
        list.addAll(AppContext.getListMedias());

        Bundle b = getArguments();

        if ( list != null && b != null && b.getString(TAG_NAME) != null){

           tagName =  b.getString(TAG_NAME);

        }

        if (list == null) {

            SongStorageAsyncTask task = new SongStorageAsyncTask(getActivity(), this);
            task.execute();
        }
        else{

            SongCasheAsyncTask task = new SongCasheAsyncTask(getActivity(), this);
            task.execute(list);
        }

        startLoading();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Media item = adapter.getItem(position);

                item.setSelected(!item.isSelected());

                adapter.update(item, position);


            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

               //

                return false;
            }
        });



        return view;
    }


    @Override
    public void addMedia(Media item) {

        adapter.addMedia(item);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingFinish() {

        endLoading();
    }

    @Override
    public void showMedia(List<Media> list) {

        adapter.addAllMedia(list);
        adapter.notifyDataSetChanged();

        endLoading();
    }

    @Override
    public void startLoading() {

        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void endLoading() {

        relativeLayout.setVisibility(View.GONE);

       /* cbListner = null;

        cbListner = new CbMultSongsListner();*/

        List<Media> listTagSongs = ShPrefUtils.getPlayListByTagName(getActivity(), tagName);
        adapter.setSaveSongs(listTagSongs);
    }

    public void addSongs() {

        goBack(tagName, cbListner.getListMedia());
        adapter.clearChecks();
        cbListner.clearMedias();

    }

    @Override
    public void onResume() {
        super.onResume();

    }
}

