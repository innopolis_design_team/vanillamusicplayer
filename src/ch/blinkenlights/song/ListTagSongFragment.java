package ch.blinkenlights.song;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.artist.IViewMedia;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.objects.VanMediaPlayer;

public class ListTagSongFragment extends Fragment implements IMediaLoader, IViewMedia, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener {

    private static final String TAG_NAME = "tag_name";
    private SongAdapter adapter;
    private ListView listView;
    private RelativeLayout relativeLayout;
    private TextView trackTitle;
    private TextView trackArtist;
    private ImageView trackArtwork;
    private int itemPosition;
    private Button musicButton;
    private List<Media> listMedias;


    VanMediaPlayer player;

    public static ListTagSongFragment newInstance(String tagName) {

        Bundle b = new Bundle();
        b.putString(TAG_NAME, tagName);

        ListTagSongFragment fragment = new ListTagSongFragment();

        fragment.setArguments(b);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_view_item, container, false);

        listView = (ListView) view.findViewById(R.id.listView);

        relativeLayout = (RelativeLayout) view.findViewById(R.id.rlArtist);

        adapter = new SongAdapter(getActivity());

        listView.setAdapter(adapter);

        this.itemPosition = 0;

        listMedias = new ArrayList<>();

        Media item = new Media("Hello",null,"Hello","Hello", "Hello", 1,"Hello" );

        listMedias.add(item);

        player = VanMediaPlayer.getInstance(getActivity(), this, this);

        Bundle b = getArguments();

        if ( b != null && b.getString(TAG_NAME) != null) {

            String tagName = b.getString(TAG_NAME);

            listMedias = ShPrefUtils.getPlayListByTagName(getActivity(), tagName);

        }

        if (listMedias != null) {

            SongCasheAsyncTask task = new SongCasheAsyncTask(getActivity(), this);
            task.execute(listMedias);
        } else {


        }

        startLoading();
        trackTitle = (TextView) getActivity().findViewById(R.id.selected_track_title);
        trackArtist = (TextView) getActivity().findViewById(R.id.selected_track_artist);
        musicButton = (Button) getActivity().findViewById(R.id.player_play_pause);
        trackArtwork = (ImageView) getActivity().findViewById(R.id.selected_track_image);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemPosition = position;

                adapter.changlePlayState(position);

                Media item = adapter.getItem(position);

               player.playOrPause(item.getPath());
                if (item.isPlay()) {
                    musicButton.setBackgroundResource(R.drawable.widget_pause);
                } else {
                    musicButton.setBackgroundResource(R.drawable.widget_play);
                }
                trackTitle.setText(item.getTitle());
                trackArtist.setText(item.getArtist());
                if (item.getArtwork() != null) {
                    trackArtwork.setImageBitmap(item.getArtwork());
                } else {
                    Bitmap emptyCover = BitmapFactory.decodeResource(getResources(),R.drawable.empty_album2);
                    trackArtwork.setImageBitmap(emptyCover);
                }
            }
        });

        musicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.changlePlayState(itemPosition);
                Media item = adapter.getItem(itemPosition);
                player.playOrPause(item.getPath());
                if (item.isPlay()) {
                    musicButton.setBackgroundResource(R.drawable.widget_pause);
                } else {
                    musicButton.setBackgroundResource(R.drawable.widget_play);
                }
            }
        });

        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (player != null) {

            player.onDestroy();
        }
    }

    @Override
    public void addMedia(Media item) {

        adapter.addMedia(item);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingFinish() {

        endLoading();
    }

    @Override
    public void showMedia(List<Media> list) {

        adapter.addAllMedia(list);
        adapter.notifyDataSetChanged();

        endLoading();
    }

    @Override
    public void startLoading() {

        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void endLoading() {

        relativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

    }
}

