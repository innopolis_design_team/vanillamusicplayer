package ch.blinkenlights.song;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.MediaFetcher;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.objects.AppContext;


public class SongStorageAsyncTask extends AsyncTask<Void, Media, Void> {

    Activity activity;
    MediaFetcher mediaFetcher;
    IMediaLoader loader;

    public SongStorageAsyncTask(Activity activity, IMediaLoader loader) {

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected Void doInBackground(Void... params) {

        mediaFetcher = new MediaFetcher();

        List<Media> list = new ArrayList<>();

        ContentResolver cr = activity.getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);
        int count = 0;


        if (cur != null) {
            count = cur.getCount();

            if (count > 0) {
                while (cur.moveToNext()) {
                    String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                    mediaFetcher.setDataSource(data);

                    Media song = new Media(
                            mediaFetcher.getAlbum(),
                            mediaFetcher.getArtwork(),
                            mediaFetcher.getArtist(),
                            mediaFetcher.getTitle(),
                            mediaFetcher.getGenre(),
                            mediaFetcher.getYear(),
                            mediaFetcher.getDataSource()
                    );

                    list.add(song);

                    publishProgress(song);
                }

            }
        }
        cur.close();

        AppContext.setListMedias(list);

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        loader.onLoadingFinish();
    }

    @Override
    protected void onProgressUpdate(Media... values) {
        super.onProgressUpdate(values);

        Media item = values[0];
        loader.addMedia(item);
    }
}
