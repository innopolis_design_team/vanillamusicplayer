package ch.blinkenlights.song;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.ArtistDto;


public class SongAdapter extends BaseAdapter {

    private List<Media> medias;
    private LayoutInflater layoutInflater;
    private Context context;

    SongAdapter(Context context, List<Media> medias) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = medias;
        this.context = context;
    }

    public SongAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = new ArrayList<>();
        this.context = context;
    }

    public void addMedia(Media item){

        this.medias.add(item);
    }

    public void addAllMedia(List<Media> list){

        this.medias.addAll(list);
    }

    public List<Media> getListMedias(){

        return this.medias;
    }

    @Override
    public int getCount() {
        return medias.size();
    }

    @Override
    public Media getItem(int position) {
        return medias.get(position);
    }

    public void changlePlayState(int position){

        boolean state = medias.get(position).isPlay();

        for (int i = 0; i < medias.size(); i++){

            if (medias.get(i).isPlay()){

                medias.get(i).setPlay(false);
            }
        }


        medias.get(position).setPlay(!state);

        notifyDataSetChanged();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView artist;
        TextView title;
        LinearLayout layout;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.song_item, parent, false);
            convertView.setTag(R.id.song_item_title, convertView.findViewById(R.id.song_item_title));
            convertView.setTag(R.id.song_item_artist, convertView.findViewById(R.id.song_item_artist));
            convertView.setTag(R.id.songs_ll, convertView.findViewById(R.id.songs_ll));
        }

        title = (TextView) convertView.getTag(R.id.song_item_title);
        artist = (TextView) convertView.getTag(R.id.song_item_artist);
        layout = (LinearLayout) convertView.getTag(R.id.songs_ll);

        Media item = getItem(position);
        artist.setText(item.getArtist());
        title.setText(item.getTitle());

        if (item.isPlay()){

            layout.setBackgroundResource(android.R.color.holo_blue_dark);
            artist.setTextColor(Color.WHITE);
            title.setTextColor(Color.WHITE);
        }
        else {

            layout.setBackgroundResource(R.color.main_color);
            artist.setTextColor(Color.BLACK);
            title.setTextColor(context.getResources().getColor(R.color.material_drawer_secondary_text));
        }

        if ( item.getTitle() == null && item.getArtist() == null && item.getAlbum() != null){

           artist.setText(item.getAlbum());
        }

        return convertView;
    }

    public List<Media> getCurrentPlaylist() {
        return new ArrayList<Media>(this.medias);
    }

    public void setCurrentPlaylist(List<Media> playlist) {

        if (playlist == null) {

            throw new IllegalArgumentException();

        }

        this.medias.clear();
        this.medias.addAll(playlist);
        notifyDataSetChanged();

    }

    public int changlePlayState(Media item) {

        int index = 0;
        for (int i = 0; i < medias.size(); i++){

            Media media = medias.get(i);

            if (media.getPath().equalsIgnoreCase(item.getPath())){

                changlePlayState(i);
                index = i;
            }
        }

        notifyDataSetChanged();
        return index;
    }

    public void clearAllSelections() {

        for (int i = 0; i < medias.size(); i++){

                medias.get(i).setPlay(false);
        }

        notifyDataSetChanged();
    }
}

