package ch.blinkenlights.song;

import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.artist.IViewMedia;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.objects.MediaLoader;
import ch.blinkenlights.objects.VanMediaPlayer;
import ch.blinkenlights.player.FullPlayer;
import ch.blinkenlights.playlist.PlayListAdapter;

public class ListSongFragment extends Fragment implements IMediaLoader, IViewMedia, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener {

    private static final String ALBUM_NAME = "album_name";
    private SongAdapter adapter;

    private ListView listView;
    private RelativeLayout relativeLayout;
    private TextView trackTitle;
    private TextView trackArtist;
    private ImageView trackArtwork;
    private int itemPosition;
    private Button musicButton;
    private Media item;
    private List<Media> list;
    public final static String MEDIA_SONG = "media_song";

    private List<Media> fullPlaylist;
    private boolean inSearchMode = false;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    VanMediaPlayer player;
    private int CODE_FULL_PLAYER = 100;

    public static ListSongFragment newInstance(String albumName) {

        Bundle b = new Bundle();
        b.putString(ALBUM_NAME, albumName);

        ListSongFragment fragment = new ListSongFragment();

        fragment.setArguments(b);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_view_item, container, false);

        listView = (ListView) view.findViewById(R.id.listView);

        relativeLayout = (RelativeLayout) view.findViewById(R.id.rlArtist);

        adapter = new SongAdapter(getActivity());

        listView.setAdapter(adapter);

        fullPlaylist = new ArrayList<Media>();

        this.itemPosition = 0;

        list = AppContext.getListMedias();

        if (list == null) {

            list = MediaLoader.getListMedias(getActivity());
            AppContext.setListMedias(list);
        }
        player = VanMediaPlayer.getInstance(getActivity(), this, this);


        Bundle b = getArguments();

        if (list != null && b != null && b.getString(ALBUM_NAME) != null) {

            MediaStorage storage = MediaStorage.getInstance(getActivity());

            String albumName = b.getString(ALBUM_NAME);
            list = storage.getSongsByAlbumName(list, albumName);

        }

        if (list == null) {

            SongStorageAsyncTask task = new SongStorageAsyncTask(getActivity(), this);
            task.execute();
        } else {

            SongCasheAsyncTask task = new SongCasheAsyncTask(getActivity(), this);
            task.execute(list);
        }

        startLoading();
        trackTitle = (TextView) getActivity().findViewById(R.id.selected_track_title);
        trackArtist = (TextView) getActivity().findViewById(R.id.selected_track_artist);
        musicButton = (Button) getActivity().findViewById(R.id.player_play_pause);
        trackArtwork = (ImageView) getActivity().findViewById(R.id.selected_track_image);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                onLvItemCLick(position);
            }
        });

        musicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.changlePlayState(itemPosition);
                item = adapter.getItem(itemPosition);
                player.playOrPause(item.getPath());
                if (item.isPlay()) {
                    musicButton.setBackgroundResource(R.drawable.widget_pause);
                } else {
                    musicButton.setBackgroundResource(R.drawable.widget_play);
                }
            }
        });

        trackArtwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Media media = new Media(item.getAlbum(), null, item.getArtist(), item.getTitle(), item.getGenre(), item.getYear(), item.getPath());
                media.setCurrentPlayPosition(player.getCurrentPlayPosition());
                media.setPlay(player.isPlaying());


                Intent intent = new Intent(getActivity(), FullPlayer.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(MEDIA_SONG, media);
                startActivityForResult(intent, CODE_FULL_PLAYER);
                player.stop();

                onLvItemCLick(itemPosition);

                ((Activity) container.getContext()).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        return view;
    }

    private void onLvItemCLick(int position) {

        itemPosition = position;

        adapter.changlePlayState(position);

        item = adapter.getItem(position);

        if (item.getArtwork() == null) {
            item.setArtwork(MediaLoader.getFullMediaInfo(item.getPath()).getArtwork());
        }

        player.playOrPause(item.getPath());
        setSongData(item);
    }


    private void setSongData(Media item){

        if (item.getArtwork() == null){

            item.setArtwork(MediaLoader.getFullMediaInfo(item.getPath()).getArtwork());
        }

        if (item.isPlay()) {
            musicButton.setBackgroundResource(R.drawable.widget_pause);
        } else {
            musicButton.setBackgroundResource(R.drawable.widget_play);
        }
        trackTitle.setText(item.getTitle());
        trackArtist.setText(item.getArtist());
        if (item.getArtwork() != null) {
            trackArtwork.setImageBitmap(item.getArtwork());
        } else {
            Bitmap emptyCover = BitmapFactory.decodeResource(getResources(),R.drawable.empty_album2);
            trackArtwork.setImageBitmap(emptyCover);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == CODE_FULL_PLAYER && resultCode == getActivity().RESULT_OK && intent != null) {


                item = (Media) intent.getSerializableExtra(FullPlayer.FULL_ITEM);

            if (item.isPlay()) {

                item.setPlay(false);

                adapter.clearAllSelections();

                itemPosition = adapter.changlePlayState(item);

                player.playOrPause(item.getPath());

                player.setCurrentPlayPosition(item.getCurrentPlayPosition());

                item.setPlay(true);
            }
               setSongData(item);

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String filterString) {

                    if (inSearchMode && (filterString == null || filterString.length() == 0)) {

                        inSearchMode = false;
                        adapter.setCurrentPlaylist(fullPlaylist);
                        fullPlaylist.clear();
                        return true;

                    }
                    else if (!inSearchMode) {

                        inSearchMode = true;
                        fullPlaylist = adapter.getCurrentPlaylist();

                    }

                    adapter.setCurrentPlaylist(PlayListAdapter.filterPlaylist(fullPlaylist, filterString));

                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // Not implemented here
                return false;
            default:
                break;
        }
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (player != null) {

            player.onDestroy();
        }
    }

    @Override
    public void addMedia(Media item) {

        adapter.addMedia(item);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingFinish() {

        endLoading();
    }

    @Override
    public void showMedia(List<Media> list) {

        adapter.addAllMedia(list);
        adapter.notifyDataSetChanged();

        endLoading();
    }

    @Override
    public void startLoading() {

        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void endLoading() {

        relativeLayout.setVisibility(View.GONE);

        AppContext.setCurrentPlayList(adapter.getListMedias());


    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

    }
}

