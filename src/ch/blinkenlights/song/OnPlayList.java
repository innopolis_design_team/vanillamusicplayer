package ch.blinkenlights.song;

import ch.blinkenlights.activities.Media;

/**
 * Created by galie on 03.11.2016.
 */

public interface OnPlayList {

    void playSong (Media item);
}
