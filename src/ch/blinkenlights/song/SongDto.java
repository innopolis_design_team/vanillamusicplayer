package ch.blinkenlights.song;

import java.util.HashSet;

import ch.blinkenlights.tags.Tag;

/**
 * Created by andre on 03.10.2016.
 */

public class SongDto {

    public SongDto() {

        tags = new HashSet<Tag>();

    }

    private String name;

    private HashSet<Tag> tags;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashSet<Tag> getTags() { return tags; }

    public void addTag(Tag tag) {

        tags.add(tag);

    }

}
