package ch.blinkenlights.song;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;
import ch.blinkenlights.artist.IMediaLoader;

/**
 * Created by andre on 15.10.2016.
 */

public class SongCasheAsyncTask extends AsyncTask<Object, Object, List<Media>> {

    Activity activity;
    IMediaLoader loader;

    public SongCasheAsyncTask(Activity activity, IMediaLoader loader) {

        this.activity = activity;
        this.loader = loader;
    }

    @Override
    protected List<Media> doInBackground(Object... params) {

        Object o1 = params[0];

        List<Media> listAll = (List<Media>) o1;

        return listAll;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(List<Media> result) {
        super.onPostExecute(result);


        loader.showMedia(result);
        loader.onLoadingFinish();
    }
}
