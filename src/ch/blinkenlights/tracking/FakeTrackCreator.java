package ch.blinkenlights.tracking;


import android.content.Intent;

import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.blinkenlights.playlist.PlayListDto;

public class FakeTrackCreator {

    public static HashMap<Integer, List<PlayListDto>> fakePlayLists(){

        int [] array = {DetectedActivity.IN_VEHICLE, DetectedActivity.ON_BICYCLE, DetectedActivity.ON_FOOT,
                DetectedActivity.RUNNING, DetectedActivity.STILL, DetectedActivity.TILTING, DetectedActivity.WALKING, DetectedActivity.UNKNOWN };

        HashMap<Integer, List<PlayListDto>> map = new HashMap<>();

        int index = 1;

        String playListName = "";

        for (int type : array){

            switch( type ) {
                case DetectedActivity.IN_VEHICLE: {
                    playListName = "In Vehicle";
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    playListName = "On Bicycle";
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    playListName = "On Foot";
                    break;
                }
                case DetectedActivity.RUNNING: {
                    playListName = "Running";
                    break;
                }
                case DetectedActivity.STILL: {
                    playListName = "Still";
                    break;
                }
                case DetectedActivity.TILTING: {
                    playListName = "Tilting";
                    break;
                }
                case DetectedActivity.WALKING: {
                    playListName = "Walking";
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    playListName = "Unknown";
                    break;
                }
            }

            int size = index % 3;

            if (size == 0){

                size = 3;
            }

            PlayListDto item = null;
            List<PlayListDto> listPlayLists = new ArrayList<>();

            for ( int i = 1; i <= size; i++){

                item = new PlayListDto();
                item.setFullName("[" + playListName.toUpperCase() + "] " + " Playlist" + i);
                item.setShortName(playListName);

                listPlayLists.add(item);
            }

            map.put(type, listPlayLists);
            index++;

        }

        return map;
    }

    public static List<TrackingDto> fakeUserActions(){

        List<TrackingDto> list = new ArrayList<>();

        TrackingDto item = null;

        item = new TrackingDto();
        item.setType(DetectedActivity.IN_VEHICLE);
        item.setConfidence(30);

        list.add(item);

        item = new TrackingDto();
        item.setType(DetectedActivity.ON_BICYCLE);
        item.setConfidence(40);

        list.add(item);

        item = new TrackingDto();
        item.setType(DetectedActivity.ON_FOOT);
        item.setConfidence(60);

        list.add(item);

        item = new TrackingDto();
        item.setType(DetectedActivity.RUNNING);
        item.setConfidence(50);

        list.add(item);


        item = new TrackingDto();
        item.setType(DetectedActivity.STILL);
        item.setConfidence(45);

        list.add(item);


        item = new TrackingDto();
        item.setType(DetectedActivity.TILTING);
        item.setConfidence(80);

        list.add(item);


        item = new TrackingDto();
        item.setType(DetectedActivity.WALKING);
        item.setConfidence(55);

        list.add(item);

        item = new TrackingDto();
        item.setType(DetectedActivity.UNKNOWN);
        item.setConfidence(10);

        list.add(item);

        return list;
    }
}
