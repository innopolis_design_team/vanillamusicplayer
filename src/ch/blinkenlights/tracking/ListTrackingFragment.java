package ch.blinkenlights.tracking;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.activities.TrackActivity;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.playlist.PlayListAdapter;
import ch.blinkenlights.playlist.PlayListDto;
import ch.blinkenlights.song.ListTagSongFragment;

public class ListTrackingFragment extends ListFragment {

    private PlayListAdapter adapter;

    List<TrackingDto> trackList = new ArrayList<>();

    private PlayListDto currentPlaylist;
    private PlayListDto prevPlaylist;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<PlayListDto> listPlayLists = ShPrefUtils.getActionPlayLists(getActivity());

         adapter = new PlayListAdapter(getActivity(), listPlayLists);

        setListAdapter(adapter);
    }

    private String getResValue(int id){

        return getActivity().getResources().getString(id);
    }

    private void fillTrackList(DetectedActivity activity, int id, List<String> listTags){

        TrackingDto trackingDto = null;

        final String name = getResValue(id);

        if (listTags.contains(name)){

            trackingDto = new TrackingDto();

            trackingDto.setName(name);

            trackingDto.setConfidence(activity.getConfidence());

            trackList.add(trackingDto);

        }

    }

    private void modifyAdapter() {

        adapter.changeTagsPositions(trackList);

        currentPlaylist = adapter.getTopPlayList();

        startPlaySongs();

        trackList = new ArrayList<>();
        trackList.clear();

    }

    private void startPlaySongs(){

        if (prevPlaylist != null && currentPlaylist != null){

            if (currentPlaylist.getShortName().equalsIgnoreCase(prevPlaylist.getShortName())){

                if (getActivity() instanceof TrackActivity){

                    ((TrackActivity)getActivity()).playSong(currentPlaylist);
                }

            }
            else {

                prevPlaylist = currentPlaylist;
            }

        }
        else {

            prevPlaylist = adapter.getTopPlayList();
        }

    }


    public void changeTagsPositions(List<DetectedActivity> probableActivities){

        if (probableActivities == null) { return;}

        List<String> listTags = adapter.getTagsNames();



        for( DetectedActivity activity : probableActivities ) {
            switch( activity.getType() ) {
                case DetectedActivity.IN_VEHICLE: {

                  fillTrackList(activity, R.string.on_vehicle, listTags);
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    fillTrackList(activity, R.string.on_bycycle, listTags);
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    fillTrackList(activity, R.string.walking, listTags);
                    break;
                }
                case DetectedActivity.RUNNING: {
                 //   sb.append( "Running: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.STILL: {
                 //   sb.append( "Still: " + activity.getConfidence() + "\n" );
                    fillTrackList(activity, R.string.still, listTags);
                    break;
                }
                case DetectedActivity.TILTING: {
                    fillTrackList(activity, R.string.tilting, listTags);
                    break;
                }
                case DetectedActivity.WALKING: {
                  //  sb.append( "Walking: " + activity.getConfidence() + "\n" );
                    fillTrackList(activity, R.string.walking, listTags);
                    if( activity.getConfidence() >= 75 ) {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
                        builder.setContentText( "Are you walking?" + "\n" );
                        builder.setSmallIcon( R.drawable.icon );
                        builder.setContentTitle( getString( R.string.app_name ) );
                        NotificationManagerCompat.from(getActivity()).notify(0, builder.build());
                    }
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                   // sb.append( "Unknown: " + activity.getConfidence() + "\n" );
                    break;
                }
            }
        }

        modifyAdapter();

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String tagName = adapter.getItem(position).getShortName();

        ListTagSongFragment fragment = ListTagSongFragment.newInstance(tagName);

        FragmentHelper.setFragmentWithBackStack(getActivity(), fragment);

    }
}
