package ch.blinkenlights.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.song.ListMultiSongFragment;

/**
 * Created by galie on 19.10.2016.
 */

public class TagSongActivity extends ActionBarActivity {


    private String TAG_NAME = "tag_name";
    private String TAG_SONGS = "tag_songs";

    String tagName = "";
    Fragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.child_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle b =  getIntent().getExtras();

        if ( b!= null && b.getString(TAG_NAME) != null){

            fragment = ListMultiSongFragment.newInstance(b.getString(TAG_NAME));
            getSupportActionBar().setTitle(b.getString(TAG_NAME));
        }

        FragmentHelper.setFragment(TagSongActivity.this, fragment);



       /* TextView tv = (TextView) findViewById(R.id.atrist_tv_name);

        Intent intent = getIntent();

        if (intent != null) {

            tagName = intent.getStringExtra(TAG_NAME);

            tv.setText(tagName);
        }

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goBack();
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mult, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_add_song) {

            if (fragment instanceof ListMultiSongFragment){

                ( (ListMultiSongFragment) fragment).addSongs();
            }

            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    public void goBack(String tagName, ArrayList<Media> list) {
        Intent intent = new Intent();
        intent.putExtra(TAG_SONGS, list);
        intent.putExtra(TAG_NAME, tagName);
        setResult(RESULT_OK, intent);
        finish();
    }
}
