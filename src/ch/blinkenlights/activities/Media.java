package ch.blinkenlights.activities;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by maci on 10/5/16.
 */

public class Media implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;

    private String album;
    private Bitmap artwork;
    private String title;
    private String genre;
    private Integer year;
    private String path;
    private String artist;
    private int currentPlayPosition;
    private boolean isPlay = false;
    private boolean isSelected = false;




    public Media(
            String album,
            Bitmap artwork,
            String artist,
            String title,
            String genre,
            Integer year,
            String path
    ) {
        this.album = album;
        this.artwork = artwork;
        this.artist = artist;
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.path = path;
    }

    public String getAlbum() {
        return this.album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Bitmap getArtwork() {
        return this.artwork;
    }

    public void setArtwork(Bitmap artwork) {
        this.artwork = artwork;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String track) {
        this.genre = genre;
    }

    public Integer getYear() {
        return this.year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isPlay() {
        return isPlay;
    }

    public void setPlay(boolean play) {
        isPlay = play;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getCurrentPlayPosition() {
        return currentPlayPosition;
    }

    public void setCurrentPlayPosition(int currentPlayPosition) {
        this.currentPlayPosition = currentPlayPosition;
    }
}
