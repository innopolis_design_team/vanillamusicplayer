package ch.blinkenlights.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;

import ch.blinkenlights.android.vanilla.R;

/**
 * Created by maci on 10/6/16.
 */

public class MediaFetcher {

    private MediaMetadataRetriever metaRetriver;
    private String dataSource;

    public MediaFetcher() {
        this.metaRetriver = new MediaMetadataRetriever();
    }

    /*
     * Get album artwork
     * if picture not exists return null
     */
    public Bitmap getArtwork() {
        try {
            Bitmap songImage;
            if (getAlbum() != "Unknown") {
                byte[] art = metaRetriver.getEmbeddedPicture();
                songImage = BitmapFactory
                        .decodeByteArray(art, 0, art.length);
                return songImage;
            } else {
                return null;
            }
        } catch (Exception e) {
            //default album art for unknown
            return null;
        }
    }

    public String getAlbum() {
        try {
            String album = this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            if (album != null) {
                return album;
            }
            return "Unknown";
        } catch (Exception e) {
            return null;
        }
    }

    public String getArtist() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        } catch (Exception e) {
            return null;
        }
    }

    public String getTitle() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        } catch (Exception e) {
            return null;
        }
    }

    public Integer getYear() {
        try {
            return Integer.parseInt(this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
        } catch (Exception e) {
            return null;
        }
    }

    public String getGenre() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
        } catch (Exception e) {
            return null;
        }
    }

    public String getDataSource() {
        return this.dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
        this.metaRetriver.setDataSource(dataSource);
    }
}
