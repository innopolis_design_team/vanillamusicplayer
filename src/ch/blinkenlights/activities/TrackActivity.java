package ch.blinkenlights.activities;


import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.objects.VanMediaPlayer;
import ch.blinkenlights.playlist.PlayListDto;
import ch.blinkenlights.song.ListSongFragment;
import ch.blinkenlights.song.OnPlayList;
import ch.blinkenlights.tracking.ActivityRecognizedService;
import ch.blinkenlights.tracking.ListTrackingFragment;

public class TrackActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener, OnPlayList {

    private PlayListDto playingCurrentPlayList = null;

    public void playSong(PlayListDto playList) {

        if (playingCurrentPlayList == null || !playingCurrentPlayList.getShortName()
                .equalsIgnoreCase(playList.getShortName())) {

            player.stop();

            List<Media> listSongs = ShPrefUtils.getPlayListByTagName(TrackActivity.this, playList.getShortName());

            if (listSongs != null && !listSongs.isEmpty()) {

                Media item = listSongs.get(0);


                player.playOrPause(item.getPath());

                if (player.isPlaying()) {
                    musicButton.setBackgroundResource(R.drawable.widget_pause);
                } else {
                    musicButton.setBackgroundResource(R.drawable.widget_play);
                }
                trackTitle.setText(item.getTitle());
                trackArtist.setText(item.getArtist());
                if (item.getArtwork() != null) {
                    trackArtwork.setImageBitmap(item.getArtwork());
                } else {
                    trackArtwork.setBackgroundResource(R.drawable.album_art_empty);
                }

                musicButton.setTag(R.id.player_play_pause, item);
            }

            playingCurrentPlayList = playList;
        }

    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

    }

    @Override
    public void playSong(Media item) {

    }

    private enum State {Show, Hide}

    private static final int TRACKING_INTERVAL = 5000;
    private static List<DetectedActivity> probableActivities;

    private State state = State.Hide;
    private Thread thread;
    private Object lock = new Object();

    public GoogleApiClient mApiClient;

    public static void setProbableActivities(List<DetectedActivity> probableActivities) {
        TrackActivity.probableActivities = probableActivities;
    }

    private ListTrackingFragment fragment;

    private TextView trackTitle;
    private TextView trackArtist;
    private ImageView trackArtwork;
    private int itemPosition;
    private Button musicButton;
    VanMediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        trackTitle = (TextView) findViewById(R.id.selected_track_title);

        this.itemPosition = 0;

        trackTitle = (TextView) findViewById(R.id.selected_track_title);
        trackArtist = (TextView) findViewById(R.id.selected_track_artist);
        musicButton = (Button) findViewById(R.id.player_play_pause);
        trackArtwork = (ImageView) findViewById(R.id.selected_track_image);

        player = VanMediaPlayer.getInstance(TrackActivity.this, this, this, this);


        fragment = new ListTrackingFragment();

        FragmentHelper.setFragment(TrackActivity.this, fragment);

       /* startStopButton = (Button) findViewById(R.id.StartButton);

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStopButton = (Button) findViewById(R.id.StartButton);
                StartOrStopTracking(startStopButton);
            }
        });*/

        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mApiClient.connect();

        StartOrStopTracking();

        musicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Media item = (Media) musicButton.getTag(R.id.player_play_pause);
                player.playOrPause(item.getPath());

                if (player.isPlaying()) {
                    musicButton.setBackgroundResource(R.drawable.widget_pause);
                } else {
                    musicButton.setBackgroundResource(R.drawable.widget_play);
                }
            }
        });
    }

    private void StartOrStopTracking() {
        if (state == State.Hide) {
            state = State.Show;


            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                private long startTime = System.currentTimeMillis();

                public void run() {
                    synchronized (lock) {
                        while (state == State.Show) {
                            handler.post(new Runnable() {
                                public void run() {
                                    handleDetectedActivities();
                                }
                            });
                            try {
                                lock.wait(TRACKING_INTERVAL);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        lock.notify();
                    }
                }
            };
            new Thread(runnable).start();
        } else if (state == State.Show) {
            state = State.Hide;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Intent intent = new Intent(this, ActivityRecognizedService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mApiClient, TRACKING_INTERVAL, pendingIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void handleDetectedActivities() {

        if (probableActivities == null) {
            return;
        }

        if (fragment != null) {

            fragment.changeTagsPositions(probableActivities);
        }
        StringBuilder sb = new StringBuilder();
        for (DetectedActivity activity : probableActivities) {
            switch (activity.getType()) {
                case DetectedActivity.IN_VEHICLE: {
                    sb.append("In Vehicle: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    sb.append("On Bicycle: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    sb.append("On Foot: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.RUNNING: {
                    sb.append("Running: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.STILL: {
                    sb.append("Still: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.TILTING: {
                    sb.append("Tilting: " + activity.getConfidence() + "\n");
                    break;
                }
                case DetectedActivity.WALKING: {
                    sb.append("Walking: " + activity.getConfidence() + "\n");
                    if (activity.getConfidence() >= 75) {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                        builder.setContentText("Are you walking?" + "\n");
                        builder.setSmallIcon(R.drawable.icon);
                        builder.setContentTitle(getString(R.string.app_name));
                        NotificationManagerCompat.from(this).notify(0, builder.build());
                    }
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    sb.append("Unknown: " + activity.getConfidence() + "\n");
                    break;
                }
            }
        }

        try {
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        StartOrStopTracking();

        if (player != null) {
            player.onDestroy();
        }
    }
}
