package ch.blinkenlights.activities;

import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import ch.blinkenlights.album.AlbumViewFragment;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.ArtistViewFragment;
import ch.blinkenlights.genre.GenreViewFragment;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.playlist.ListPlayListFragment;
import ch.blinkenlights.song.ListSongFragment;
import ch.blinkenlights.tag.ActivityFragment;
import ch.blinkenlights.tag.MoodFragment;
import ch.blinkenlights.tracking.ActivityRecognizedService;

import static android.R.attr.type;

public class MainActivity extends ActionBarActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int ARTIST = 5;
    private static final int ALBUM = 6;
    private static final int SONG = 7;
    private static final int PLAY_LIST = 3;
    private static final int GENRE = 8;
    private static final int TAG = 1;
    private static final int TRACK = 2;

    Drawer drawer;
    public GoogleApiClient mApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout);
        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawer = new Drawer();
        drawer.withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_tags).withIcon(FontAwesome.Icon.faw_tag).withIdentifier(6),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_tracking).withIcon(FontAwesome.Icon.faw_user_times).withIdentifier(7),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_moods).withIcon(FontAwesome.Icon.faw_headphones).withIdentifier(4),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_artists).withIcon(FontAwesome.Icon.faw_user).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_albums).withIcon(FontAwesome.Icon.faw_file_o).withIdentifier(21),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_songs).withIcon(FontAwesome.Icon.faw_music).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_genres).withIcon(FontAwesome.Icon.faw_microphone).withIdentifier(5),
                        new DividerDrawerItem()
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        showFragment(position);
                    }
                })
                .build();
        Intent intent = getIntent();
        Integer fragmentType = -1;
        if (intent != null) {
            showFragment(intent.getIntExtra("type", fragmentType));
        } else {
            fragmentType = SONG;
        }
        showFragment(fragmentType);

        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mApiClient.connect();

    }

    Fragment fragment = null;

    private void showFragment(int type) {
        if (type == ARTIST) {
            getSupportActionBar().setTitle("Artists");
            fragment = new ArtistViewFragment();
        } else if (type == ALBUM) {
            getSupportActionBar().setTitle("Albums");
            fragment = new AlbumViewFragment();
        } else if (type == SONG) {
            getSupportActionBar().setTitle("Songs");
            fragment = new ListSongFragment();
        } else if (type == PLAY_LIST) {
            getSupportActionBar().setTitle("Moods");
            fragment = new ListPlayListFragment();

        } else if (type == GENRE) {
            getSupportActionBar().setTitle("Genres");
            fragment = new GenreViewFragment();
        } else if (type == TRACK) {
            startActivity(new Intent(MainActivity.this, TrackActivity.class));
        } else if (type == TAG) {
            getSupportActionBar().setTitle("Tags");
            startActivity(new Intent(MainActivity.this, TagActivity.class));
        }
        if (fragment == null) return;
        FragmentHelper.setFragment(MainActivity.this, fragment);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Intent intent = new Intent(this, ActivityRecognizedService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(mApiClient, 1000, pendingIntent);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onResume() {

        super.onResume();
        if (fragment == null) {
            return;
        }
        if (fragment instanceof ArtistViewFragment) {
            getSupportActionBar().setTitle("Artists");
        } else if (fragment instanceof AlbumViewFragment) {
            getSupportActionBar().setTitle("Albums");
        } else if (fragment instanceof ListSongFragment) {
            getSupportActionBar().setTitle("Songs");
        } else if (fragment instanceof ListPlayListFragment) {
            getSupportActionBar().setTitle("Songs");
        } else if (fragment instanceof GenreViewFragment) {
            getSupportActionBar().setTitle("Genres");
            fragment = new GenreViewFragment();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
