package ch.blinkenlights.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import ch.blinkenlights.album.AlbumViewFragment;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.ArtistViewFragment;
import ch.blinkenlights.genre.GenreViewFragment;
import ch.blinkenlights.playlist.ListPlayListFragment;
import ch.blinkenlights.song.ListSongFragment;
import ch.blinkenlights.tag.MoodFragment;
import ch.blinkenlights.tag.ActivityFragment;

public class TagActivity extends ActionBarActivity {
    private static final int ARTIST = 5;
    private static final int ALBUM = 6;
    private static final int SONG = 7;
    private static final int PLAY_LIST = 3;
    private static final int GENRE = 8;
    private static final int TAG = 1;
    private static final int TRACK = 2;
    FragmentTabHost mTabHost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tags");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        new Drawer()
                .withActivity(this)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_tags).withIcon(FontAwesome.Icon.faw_tag).withIdentifier(6),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_tracking).withIcon(FontAwesome.Icon.faw_user_times).withIdentifier(7),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_moods).withIcon(FontAwesome.Icon.faw_headphones).withIdentifier(4),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_artists).withIcon(FontAwesome.Icon.faw_user).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_albums).withIcon(FontAwesome.Icon.faw_file_o).withIdentifier(21),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_songs).withIcon(FontAwesome.Icon.faw_music).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_genres).withIcon(FontAwesome.Icon.faw_microphone).withIdentifier(5),
                        new DividerDrawerItem()
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        showFragment(position);
                    }
                })
                .build();

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("Activities"), ActivityFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("Moods"), MoodFragment.class, null);

        mTabHost.setCurrentTabByTag("tab1");

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                if (tabId.equalsIgnoreCase("tab1")) {

                    if (tabId.equals("tab1")) {

                        new ActivityFragment();
                    }
                } else {
                    if (tabId.equals("tab2")) {
                        new MoodFragment();
                    }
                }
            }
        });
        final TabWidget tw = (TabWidget) mTabHost.findViewById(android.R.id.tabs);
        for (int i = 0; i < tw.getChildCount(); ++i) {
            final View tabView = tw.getChildTabViewAt(i);
            final TextView tv = (TextView) tabView.findViewById(android.R.id.title);
            tv.setTextSize(16);
        }
    }

    private void showFragment(int type) {

        Fragment fragment = null;

        if (type == ARTIST) {
            getSupportActionBar().setTitle("Artists");
            fragment = new ArtistViewFragment();
        } else if (type == ALBUM) {
            getSupportActionBar().setTitle("Albums");
            fragment = new AlbumViewFragment();
        } else if (type == SONG) {
            getSupportActionBar().setTitle("Songs");
            fragment = new ListSongFragment();
        } else if (type == PLAY_LIST) {
            getSupportActionBar().setTitle("Moods");
            fragment = new ListPlayListFragment();
        } else if (type == GENRE) {
            getSupportActionBar().setTitle("Genres");
            fragment = new GenreViewFragment();
        } else if (type == TRACK) {
            startActivity(new Intent(TagActivity.this, TrackActivity.class));
        } else if (type == TAG) {
            startActivity(new Intent(TagActivity.this, TagActivity.class));
        }
        if (fragment == null) return;
        Intent intent = new Intent(TagActivity.this, MainActivity.class);
        intent.putExtra("type",type);
        startActivity(intent);
    }
}
