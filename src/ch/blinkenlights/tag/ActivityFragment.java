package ch.blinkenlights.tag;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import ch.blinkenlights.ShPrefUtils;
import ch.blinkenlights.activities.Media;
import ch.blinkenlights.activities.TagSongActivity;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.tags.TagHolder;

/**
 * Created by hp1 on 21-01-2015.
 */
public class ActivityFragment extends ListFragment {

    private String TAG_NAME = "tag_name";
    private int CODE_GET_SONGS = 1;
    private String TAG_SONGS = "tag_songs";
    private TagHolder tagsList;

    static ActivityFragment newInstance(){

        ActivityFragment fragment = new ActivityFragment();
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == CODE_GET_SONGS && resultCode == getActivity().RESULT_OK && intent != null){

            ArrayList<Media> list = (ArrayList<Media>) intent.getSerializableExtra(TAG_SONGS);

            String tagName = intent.getStringExtra(TAG_NAME);

            ShPrefUtils.savePlayList(getActivity(), tagName, list);

            String result = String.format("The playlist has been successfully added %d songs", list.size());

            Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();

        }

        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tagsList.Initialize();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.tag_list,tagsList.getTagsTitle());

        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String tagName =  adapter.getItem(position);

                Intent intent = new Intent(getActivity(), TagSongActivity.class);
                intent.putExtra(TAG_NAME, tagName);
                startActivityForResult(intent, CODE_GET_SONGS);

                Toast.makeText(getActivity(), tagName, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
