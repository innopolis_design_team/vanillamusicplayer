package ch.blinkenlights.album;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import ch.blinkenlights.activities.Media;
import ch.blinkenlights.android.vanilla.MediaStorage;
import ch.blinkenlights.android.vanilla.R;
import ch.blinkenlights.artist.IMediaImageLoader;
import ch.blinkenlights.artist.IMediaLoader;
import ch.blinkenlights.artist.IViewMedia;
import ch.blinkenlights.objects.AppContext;
import ch.blinkenlights.objects.FragmentHelper;
import ch.blinkenlights.objects.MediaLoader;
import ch.blinkenlights.playlist.PlayListAdapter;
import ch.blinkenlights.song.ListSongFragment;


public class AlbumViewFragment extends Fragment implements IMediaLoader, IMediaImageLoader, IViewMedia{

    private static final String ARTIST_NAME = "artist_name";
    private static final String GENRE_NAME = "genre_name";
    private GridView gridView;
    private AlbumLibraryAdapter adapter;

    private RelativeLayout relativeLayout;

    private List<Media> fullPlaylist;
    private boolean inSearchMode = false;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    public static AlbumViewFragment newInstance(String artistName, String genreName){

        Bundle b = new Bundle();
        if (artistName != null) {

            b.putString(ARTIST_NAME, artistName);
        }

        else if (genreName != null){

            b.putString(GENRE_NAME, genreName);
        }


        AlbumViewFragment fragment = new AlbumViewFragment();

        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_layout_main, container, false);


        relativeLayout = (RelativeLayout) view.findViewById(R.id.rlArtist);

        gridView = (GridView) view.findViewById(R.id.gridView);

        adapter = new AlbumLibraryAdapter(getActivity());

        gridView.setAdapter(adapter);

        fullPlaylist = new ArrayList<Media>();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Media item = adapter.getItem(position);

                ListSongFragment fragment = ListSongFragment.newInstance(item.getAlbum());

                ((ActionBarActivity)getActivity()).getSupportActionBar().setTitle("Songs");

                FragmentHelper.setFragmentWithBackStack(getActivity(), fragment);
            }
        });


        List<Media> list = AppContext.getListMedias();

        if (list == null){

            List<Media> listSongs = MediaLoader.getListMedias(getActivity());

            AppContext.setListMedias(listSongs);

            list = MediaLoader.getAlbums(listSongs);
        }


        Bundle b = getArguments();

        if ( list != null && b != null && b.getString(ARTIST_NAME) != null){

            MediaStorage storage = MediaStorage.getInstance(getActivity());

            String artistName = b.getString(ARTIST_NAME);
            list = storage.getAlbumsByArtistName(list, artistName);

        }
        else  if ( list != null && b != null && b.getString(GENRE_NAME) != null){

            MediaStorage storage = MediaStorage.getInstance(getActivity());

            String genreName = b.getString(GENRE_NAME);
            list = storage.getAlbumsByGenreName(list, genreName);

        }

        if (list == null) {


        }
        else{

            AlbumCasheAsyncTask task = new AlbumCasheAsyncTask(getActivity(), this);
            task.execute(list);
        }

        startLoading();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String filterString) {

                    if (inSearchMode && (filterString == null || filterString.length() == 0)) {

                        inSearchMode = false;
                        adapter.setCurrentPlaylist(fullPlaylist);
                        fullPlaylist.clear();
                        return true;

                    }
                    else if (!inSearchMode) {

                        inSearchMode = true;
                        fullPlaylist = adapter.getCurrentPlaylist();

                    }

                    adapter.setCurrentPlaylist(PlayListAdapter.filterPlaylist(fullPlaylist, filterString, PlayListAdapter.FilterMode.ALBUM));

                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void addMedia(Media item) {

        adapter.addMedia(item);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingFinish() {

        endLoading();
    }

    @Override
    public void showMedia(List<Media> list) {

        adapter.addAllMedia(list);
        adapter.notifyDataSetChanged();

        endLoading();

        AlbumStorageAsyncTask task = new AlbumStorageAsyncTask(getActivity(), this);
        task.execute(list);
    }

    @Override
    public void startLoading() {

        relativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void endLoading() {

        relativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void addImageMedia(Media item) {

        adapter.addImageMedia(item);
    }
}