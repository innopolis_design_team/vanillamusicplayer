package com.hello.maci.gridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by maci on 10/5/16.
 */

public class AlbumLibraryAdapter extends BaseAdapter {

    private List<Media> medias;
    private LayoutInflater layoutInflater;

    AlbumLibraryAdapter(Context context, List<Media> medias) {
        this.layoutInflater = LayoutInflater.from(context);
        this.medias = medias;
    }

    @Override
    public int getCount() {
        return medias.size();
    }

    @Override
    public Media getItem(int position) {
        return medias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView picture;
        TextView name;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.grid_album_item, parent, false);
            convertView.setTag(R.id.album_picture, convertView.findViewById(R.id.album_picture));
            convertView.setTag(R.id.album_text, convertView.findViewById(R.id.album_text));
        }

        picture = (ImageView) convertView.getTag(R.id.album_picture);
        name = (TextView) convertView.getTag(R.id.album_text);

        Media media = getItem(position);
        picture.setImageBitmap(media.getArtwork());
        name.setText(media.getAlbum());

        return convertView;
    }
}