package com.hello.maci.gridview;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener {

   /* HashMap<String, Media> media;
    MediaFetcher mediaFetcher;*/

     VanMediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_songs_layout);

        final String path = "/storage/sdcard1/Music/master.mp3";

        String [] array = {

                "/storage/sdcard1/Music/master.mp3",
                "/storage/sdcard1/Music/White_Rabbit.mp3",
                "/storage/sdcard1/Music/devil.mp3"

        };

        ArrayList<String> list = new ArrayList<>();

        for (String str : array){

            list.add(str);
        }

        player = VanMediaPlayer.getInstance(this, this, this);

        player.addPlayList(list);



        Button btnPlay = (Button) findViewById(R.id.btnPlay);
        Button btnStop = (Button) findViewById(R.id.btnStop);

        Button btnPause = (Button) findViewById(R.id.btnPause);
        Button btnResume = (Button) findViewById(R.id.btnResume);

        Button btnNextSong = (Button) findViewById(R.id.btnPlayNextSong);
        Button btnPrevSong = (Button) findViewById(R.id.btnPrevSong);


        btnPrevSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                player.playPreviousSong();
            }
        });

        btnNextSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                player.playNextSong();
            }
        });


        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                player.playOrPause(path);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                player.stop();
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                player.playOrPause(path);
            }
        });

        btnResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // player.resume();
            }
        });








       /* mediaFetcher = new MediaFetcher();
        media = new HashMap<String, Media>();

        ContentResolver cr = this.getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);
        int count = 0;

        if (cur != null) {
            count = cur.getCount();

            if (count > 0) {
                while (cur.moveToNext()) {
                    String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                    mediaFetcher.setDataSource(data);

                    Media song = new Media(
                            mediaFetcher.getAlbum(),
                            mediaFetcher.getArtwork(),
                            mediaFetcher.getArtist(),
                            mediaFetcher.getTitle(),
                            mediaFetcher.getGenre(),
                            mediaFetcher.getYear(),
                            mediaFetcher.getDataSource()
                    );
                    media.put(mediaFetcher.getArtist(), song);
                }

            }
        }
        cur.close();

        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new AlbumLibraryAdapter(this, new ArrayList<Media>(media.values())));*/
    }



    @Override
    public void onCompletion(MediaPlayer mp) {

       player.playNextSong();

    }

    @Override
    public void onPrepared(MediaPlayer mp) {

        mp.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        player.releaseMP();
        player.removePlayList();
    }
}
