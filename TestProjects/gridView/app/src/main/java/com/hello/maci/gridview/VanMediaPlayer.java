package com.hello.maci.gridview;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import java.util.ArrayList;

public class VanMediaPlayer {

    private static  Context contex;

    private ArrayList<String> playList;

    private static MediaPlayer mediaPlayer;

    private static int songsIndex = -1;

    private static VanMediaPlayer sInstance;

    private static AudioManager am;

    private boolean isPlaySong = false;

    private static MediaPlayer.OnPreparedListener prepListner;
    private static MediaPlayer.OnCompletionListener compltListner;

    private VanMediaPlayer(){}

    private static MediaPlayer createNewMediaPlayer(){

        mediaPlayer = new MediaPlayer();

        return mediaPlayer;
    }

    private static MediaPlayer createNewMediaPlayer(MediaPlayer.OnPreparedListener prepList, MediaPlayer.OnCompletionListener compList){

        mediaPlayer = new MediaPlayer();


        if (prepList != null) {

            mediaPlayer.setOnPreparedListener(prepList);
        }

        if (compList != null) {

            mediaPlayer.setOnCompletionListener(compList);
        }

        return mediaPlayer;
    }

    public static VanMediaPlayer getInstance(Context ctx,
                                             MediaPlayer.OnPreparedListener prepList,
                                             MediaPlayer.OnCompletionListener compList
                                             ){

        contex = ctx;

        if (sInstance == null){

            sInstance = new VanMediaPlayer();
        }

        if (mediaPlayer == null){

            mediaPlayer = createNewMediaPlayer(prepList, compList);

        }

        if (am == null){

            am = (AudioManager)contex.getSystemService(contex.AUDIO_SERVICE);
        }

        prepListner = prepList;
        compltListner = compList;



        return sInstance;
    }

    public static VanMediaPlayer getInstance(Context ctx){

        contex = ctx;

        if (sInstance == null){

            sInstance = new VanMediaPlayer();
        }

        if (mediaPlayer == null){

            mediaPlayer = createNewMediaPlayer();

        }

        if (am == null){

            am = (AudioManager)contex.getSystemService(contex.AUDIO_SERVICE);
        }

        return sInstance;
    }

    public void playPreviousSong(){

        if (playList == null) {return;}


        if (playList.size() == 0){

            return;
        }

        else if (playList.size() == 1){


            stop();
            play(playList.get(0));
        }

        else if (songsIndex <= 0){

            stop();
            play(playList.get(playList.size() - 1));
        }
        else{

            stop();
            play(playList.get(songsIndex - 1));
        }
    }

    public void playNextSong(){

        if (playList == null) {return;}


        if (playList.size() == 0){

            return;
        }

        else if (playList.size() == 1){

            stop();
            play(playList.get(0));
        }

       else if (songsIndex == playList.size() - 1){

            stop();
            play(playList.get(0));
        }
        else{

            stop();
            play(playList.get(songsIndex + 1));
        }
    }

    public void removePlayList(){

        if (playList != null) {

            this.playList.clear();
            this.playList = null;
        }

        songsIndex = -1;
        isPlaySong = false;

        prepListner = null;
        compltListner = null;
    }

    public void addPlayList(ArrayList<String> list){

        this.playList = list;

    }

    public void playOrPause(String path){

        if (!isPlaySong){

            play(path);

            isPlaySong = true;
        }
        else if (mediaPlayer.isPlaying()){

            pause();
        }
        else{

            resume();
        }
    }




    private void pause(){

        if (mediaPlayer == null) {return;}

        if (mediaPlayer.isPlaying()){

            mediaPlayer.pause();
        }

    }

    private void resume(){

        if (mediaPlayer == null) {return;}

        if (!mediaPlayer.isPlaying()) {

            mediaPlayer.start();
        }
    }

    public void stop(){

        if (mediaPlayer == null) {return;}

        mediaPlayer.stop();

        isPlaySong = false;
    }

    private void play(String path){

        if (playList != null){

            for (int i = 0; i < playList.size(); i++){

                String value = playList.get(i);

                if (value.equalsIgnoreCase(path)){

                    songsIndex = i;
                }
            }
        }

        releaseMP();
        try {

            if (mediaPlayer == null){

                mediaPlayer = createNewMediaPlayer(prepListner, compltListner);
            }

            mediaPlayer.setDataSource(path);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();

            if (!mediaPlayer.isPlaying()) {

                mediaPlayer.start();
            }
        }catch (Exception e){}
    }

    public void releaseMP() {

        isPlaySong = false;

        if (mediaPlayer != null) {

            try {

                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


}
