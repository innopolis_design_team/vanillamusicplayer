package com.hello.maci.gridview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;

/**
 * Created by maci on 10/6/16.
 */

public class MediaFetcher {

    private MediaMetadataRetriever metaRetriver;
    private String dataSource;

    public MediaFetcher() {
        this.metaRetriver = new MediaMetadataRetriever();
    }

    /*
     * Get album artwork
     * if picture not exists return null
     */
    public Bitmap getArtwork() {
        try {
            byte[] art = metaRetriver.getEmbeddedPicture();
            Bitmap songImage = BitmapFactory
                    .decodeByteArray(art, 0, art.length);
            return songImage;
        } catch (Exception e) {
            //default album art for unknown
            return null;
        }
    }

    public String getAlbum() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        } catch (Exception e) {
            return null;
        }
    }

    public String getArtist() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        } catch (Exception e) {
            return null;
        }
    }

    public String getTitle() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        } catch (Exception e) {
            return null;
        }
    }

    public Integer getYear() {
        try {
            return Integer.parseInt(this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
        } catch (Exception e) {
            return null;
        }
    }

    public String getGenre() {
        try {
            return this.metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
        } catch (Exception e) {
            return null;
        }
    }

    public String getDataSource() {
        return this.dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
        this.metaRetriver.setDataSource(dataSource);
    }
}
