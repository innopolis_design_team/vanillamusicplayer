package com.vanilla.activitytest;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private enum State { Show, Hide }

    private static final int TRACKING_INTERVAL = 1000;
    private static List<DetectedActivity> probableActivities;

    private Button startStopButton;
    private State state = State.Hide;
    private Thread thread;
    private Object lock = new Object();

    public GoogleApiClient mApiClient;

    public static void setProbableActivities(List<DetectedActivity> probableActivities) {
        MainActivity.probableActivities = probableActivities;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startStopButton = (Button) findViewById(R.id.StartButton);

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStopButton = (Button) findViewById(R.id.StartButton);
                StartOrStopTracking(startStopButton);
            }
        });

        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mApiClient.connect();
    }

    private void StartOrStopTracking(Button startStopButton) {
        if (state == State.Hide) {
            state = State.Show;
            startStopButton.setText("Stop tracking");

            final TextView textView = (TextView) findViewById(R.id.textView);

            final Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                private long startTime = System.currentTimeMillis();
                public void run() {
                    synchronized (lock) {
                        while (state == State.Show) {
                            handler.post(new Runnable() {
                                public void run() {
                                    handleDetectedActivities();
                                }
                            });
                            try {
                                lock.wait(TRACKING_INTERVAL);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        lock.notify();
                    }
                }
            };
            new Thread(runnable).start();
        } else if (state == State.Show) {
            state = State.Hide;
            startStopButton.setText("Start tracking");
            final TextView textView = (TextView) findViewById(R.id.textView);
            textView.setText("Here will be the state of tracking");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Intent intent = new Intent( this, ActivityRecognizedService.class );
        PendingIntent pendingIntent = PendingIntent.getService( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates( mApiClient, 1000, pendingIntent );
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void handleDetectedActivities() {
        final TextView textView = (TextView) findViewById(R.id.textView);

        if (probableActivities == null) {
            textView.setText("No activities detected yet.");
            return;
        }

        StringBuilder sb = new StringBuilder();
        for( DetectedActivity activity : probableActivities ) {
            switch( activity.getType() ) {
                case DetectedActivity.IN_VEHICLE: {
                    sb.append( "In Vehicle: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    sb.append( "On Bicycle: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    sb.append( "On Foot: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.RUNNING: {
                    sb.append( "Running: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.STILL: {
                    sb.append( "Still: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.TILTING: {
                    sb.append( "Tilting: " + activity.getConfidence() + "\n" );
                    break;
                }
                case DetectedActivity.WALKING: {
                    sb.append( "Walking: " + activity.getConfidence() + "\n" );
                    if( activity.getConfidence() >= 75 ) {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                        builder.setContentText( "Are you walking?" + "\n" );
                        builder.setSmallIcon( R.mipmap.ic_launcher );
                        builder.setContentTitle( getString( R.string.app_name ) );
                        NotificationManagerCompat.from(this).notify(0, builder.build());
                    }
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    sb.append( "Unknown: " + activity.getConfidence() + "\n" );
                    break;
                }
            }
        }

        try {
            textView.setText(sb.toString());
        } catch (Exception e) {

        }
    }
}
